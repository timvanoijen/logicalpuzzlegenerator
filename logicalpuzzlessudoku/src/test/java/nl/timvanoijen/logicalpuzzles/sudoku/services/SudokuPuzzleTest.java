package nl.timvanoijen.logicalpuzzles.sudoku.services;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.timvanoijen.logicalpuzzles.sudoku.SudokuDependencyInjectionModule;
import nl.timvanoijen.logicalpuzzles.sudoku.models.Sudoku;
import nl.timvanoijen.logicalpuzzles.sudoku.models.SudokuSolution;
import nl.timvanoijen.logicalpuzzles.sudoku.models.SudokuTiling;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Objects;

class SudokuPuzzleTest {

    @Inject SudokuService.Factory sudokuServiceFactory;

    @Test
    void testSolvePuzzle1() {
        Integer[][] input = {
                { 1, 4, 0, 0 },
                { 0, 2, 0, 4 },
                { 0, 0, 2, 0 },
                { 0, 3, 0, 0 },
        };

        Integer[][] solution = {
                { 1, 4, 3, 2 },
                { 3, 2, 1, 4 },
                { 4, 1, 2, 3 },
                { 2, 3, 4, 1 },
        };

        this.doTest(input, solution, new SudokuTiling(2,2,2,2));
    }

    @Test
    void testGenerate() {
        for(var k =0; k < 20; k++) {

            SudokuTiling tiling = new SudokuTiling(2, 2, 2, 2);
            int width = tiling.getWidth();

            var service = this.sudokuServiceFactory.create();
            SudokuSolution solution1 = service.generate(tiling);
            var solution2 = service.solve(solution1.getInitialPuzzle());

            for (var i = 0; i < width; i++)
                for (var j = 0; j < width; j++)
                    assert Objects.equals(solution1.getSolvedPuzzle().getValues()[i][j],
                            solution2.getSolvedPuzzle().getValues()[i][j]);
        }
    }

    private void doTest(Integer[][] input, Integer[][] expectedResult, SudokuTiling tiling) {
        int width = tiling.getWidth();
        var transformedInput = new Integer[width][width];
        for(var i = 0; i < width; i++)
            for(var j = 0; j < width; j++)
                transformedInput[i][j] = input[i][j] == 0 ? null : input[i][j];

        Sudoku sudoku = new Sudoku(tiling, transformedInput);

        var service = this.sudokuServiceFactory.create();

        SudokuSolution solution = service.solve(sudoku);
        Integer[][] solutionSquare = solution.getSolvedPuzzle().getValues();

        for(var i = 0; i < width; i++)
            for(var j = 0; j < width; j++)
                assert Objects.equals(expectedResult[i][j], solutionSquare[i][j]);
    }

    @BeforeEach
    public void init() {
        Injector injector = Guice.createInjector(new SudokuDependencyInjectionModule());
        injector.injectMembers(this);
    }
}