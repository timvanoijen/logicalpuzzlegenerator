package nl.timvanoijen.logicalpuzzles.sudoku.models;

import lombok.Getter;

@Getter
public class SudokuTiling {
    private final int blockWidth;
    private final int nrBlocksHorizontal;
    private final int blockHeight;
    private final int nrBlocksVertical;
    private final int width;
    private final int height;

    public SudokuTiling(int blockWidth, int nrBlocksHorizontal, int blockHeight, int nrBlocksVertical) {
        this.blockWidth = blockWidth;
        this.nrBlocksHorizontal = nrBlocksHorizontal;
        this.blockHeight = blockHeight;
        this.nrBlocksVertical = nrBlocksVertical;
        this.width = blockWidth * nrBlocksHorizontal;
        this.height = blockHeight * nrBlocksVertical;
        assert this.width == this.height;
    }
}
