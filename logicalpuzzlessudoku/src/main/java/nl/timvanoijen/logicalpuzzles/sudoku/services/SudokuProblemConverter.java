package nl.timvanoijen.logicalpuzzles.sudoku.services;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.service.GridUtils;
import nl.timvanoijen.logicalpuzzles.sudoku.models.Sudoku;
import nl.timvanoijen.logicalpuzzles.sudoku.models.SudokuTiling;

public class SudokuProblemConverter {

    private final SudokuProblemProvider.Factory sudokuProblemProviderFactory;
    private final GridKeysGenerator gridKeysGenerator;
    private final SudokuTiling tiling;

    @Inject
    public SudokuProblemConverter(SudokuProblemProvider.Factory sudokuProblemProviderFactory,
                                        @Assisted GridKeysGenerator gridKeysGenerator,
                                        @Assisted SudokuTiling tiling) {
        this.sudokuProblemProviderFactory = sudokuProblemProviderFactory;
        this.gridKeysGenerator = gridKeysGenerator;
        this.tiling = tiling;
    }

    public Sudoku toSudoku(ProblemRepository problemRepository) {
        var values = GridUtils.convertGridVariablesTo2DArray(
                this.gridKeysGenerator, problemRepository);
        return new Sudoku(tiling, values);
    }

    public ProblemRepository toProblemRepository(Sudoku sudoku) {
        var problemRepository = new ProblemRepository();

        problemRepository.extend(
            this.sudokuProblemProviderFactory.create(
                    sudoku.getTiling(),
                    sudoku.getValues(),
                    gridKeysGenerator));
        return problemRepository;
    }

    public interface Factory {
        SudokuProblemConverter create(GridKeysGenerator gridKeysGenerator,
                                      SudokuTiling tiling);
    }
}
