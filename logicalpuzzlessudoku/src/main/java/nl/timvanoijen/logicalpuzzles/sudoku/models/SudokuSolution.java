package nl.timvanoijen.logicalpuzzles.sudoku.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.models.GridEntry;

import java.util.List;

@Getter
@AllArgsConstructor
public class SudokuSolution {
    private final Sudoku initialPuzzle;
    private final Sudoku solvedPuzzle;
    private final List<GridEntry> steps;
    private final int difficulty;
}
