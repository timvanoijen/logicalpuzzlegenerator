package nl.timvanoijen.logicalpuzzles.sudoku;

import com.google.inject.assistedinject.FactoryModuleBuilder;
import nl.timvanoijen.logicalpuzzles.core.DefaultDependencyInjectionModule;
import nl.timvanoijen.logicalpuzzles.sudoku.services.SudokuProblemConverter;
import nl.timvanoijen.logicalpuzzles.sudoku.services.SudokuProblemProvider;
import nl.timvanoijen.logicalpuzzles.sudoku.services.SudokuService;


public class SudokuDependencyInjectionModule extends DefaultDependencyInjectionModule {
    @Override
    protected void configure() {
        super.configure();

        // Build factories
        install(new FactoryModuleBuilder().build(SudokuService.Factory.class));
        install(new FactoryModuleBuilder().build(SudokuProblemProvider.Factory.class));
        install(new FactoryModuleBuilder().build(SudokuProblemConverter.Factory.class));
    }
}
