package nl.timvanoijen.logicalpuzzles.sudoku.services;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.ColumnProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.OptionProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.KeyBuilder;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;
import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.RowProperty;
import nl.timvanoijen.logicalpuzzles.core.solvers.DirectSolvingStrategy;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingContext;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingResult;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.service.GenerationUtils;
import nl.timvanoijen.logicalpuzzles.service.GridUtils;
import nl.timvanoijen.logicalpuzzles.service.HintList;
import nl.timvanoijen.logicalpuzzles.sudoku.models.Sudoku;
import nl.timvanoijen.logicalpuzzles.sudoku.models.SudokuSolution;
import nl.timvanoijen.logicalpuzzles.sudoku.models.SudokuTiling;

import java.util.List;
import java.util.stream.Collectors;

public class SudokuService {

    private final SolvingContext.Factory solvingContextFactory;
    private final DirectSolvingStrategy.Factory directSolvingStrategyFactory;
    private final SudokuProblemConverter.Factory sudokuProblemConverterFactory;
    private final SudokuProblemProvider.Factory sudokuProblemProviderFactory;

    @Inject
    public SudokuService(SolvingContext.Factory solvingContextFactory,
                               SudokuProblemConverter.Factory sudokuProblemConverterFactory,
                               DirectSolvingStrategy.Factory directSolvingStrategyFactory,
                               SudokuProblemProvider.Factory sudokuProblemProviderFactory) {
        this.solvingContextFactory = solvingContextFactory;
        this.sudokuProblemConverterFactory = sudokuProblemConverterFactory;
        this.directSolvingStrategyFactory = directSolvingStrategyFactory;
        this.sudokuProblemProviderFactory = sudokuProblemProviderFactory;
    }

    public SudokuSolution solve(Sudoku sudoku) {

        // Initialize
        var width = sudoku.getTiling().getWidth();
        var gridKeysGenerator = new GridKeysGenerator(StringKey.of("cell"), width, width);
        var converter = this.sudokuProblemConverterFactory
                .create(gridKeysGenerator, sudoku.getTiling());

        // Solve
        var problemRepository = converter.toProblemRepository(sudoku);
        var solvingStrategy = this.directSolvingStrategyFactory.create();
        try(var solvingContext = this.solvingContextFactory.create(problemRepository)) {
            SolvingResult solvingResult = solvingContext.solve(
                    problemRepository.getGrandProblem(), solvingStrategy);

            // TODO: create proper result object to indicate that it is not feasible
            // or not solvable.
            if (!solvingResult.isSucceeded())
                return null;
        }

        // Get solution steps
        var steps = GridUtils.convertGridVariablesToSortedGridEntries(
                gridKeysGenerator, problemRepository);

        // Create solution object
        var solutionSudoku = converter.toSudoku(problemRepository);
        return new SudokuSolution(sudoku, solutionSudoku, steps, 0);
    }

    public SudokuSolution generate(SudokuTiling tiling) {
        int width = tiling.getWidth();
        var gridKeysGenerator = new GridKeysGenerator(StringKey.of("cell"), width, width);

        // Build initial empty problem
        Sudoku sudoku = new Sudoku(tiling, new Integer[width][width]);
        var converter = this.sudokuProblemConverterFactory
                .create(gridKeysGenerator, tiling);
        var problemRepository = converter.toProblemRepository(sudoku);

        // Select puzzle variables to pick from
        var optionVars = KeyBuilder.forkFrom(gridKeysGenerator.elements())
                .fork(OptionProperty.range(1, width))
                .build().flatten().stream()
                .map(problemRepository::<BoolVar>getVariable)
                .collect(Collectors.toList());

        try(var solvingContext = this.solvingContextFactory.create(problemRepository)) {
            var solvingStrategy = this.directSolvingStrategyFactory.create();

            try(var executionContext = solvingContext.provideExecutionContext()) {
                var noHintsCheckpoint = executionContext.newVersion();

                // Generate puzzle by iteratively adding a new initial value and solving.
                List<BoolVar> initialOptionVars = GenerationUtils.generateBySelectAndSolve(solvingContext,
                        problemRepository, solvingStrategy, optionVars);

                // Translate initial option vars into hints.
                var hints = HintList.fromVars(initialOptionVars.stream()
                        .map(v -> problemRepository.<IntVar>getVariable(gridKeysGenerator.element(
                                v.getKey().get(ColumnProperty.extract()),
                                v.getKey().get(RowProperty.extract())
                        ))).collect(Collectors.toList()));

                // Reduce number of hints
                executionContext.rollbackToVersion(noHintsCheckpoint - 1);
                var reducedHints = GenerationUtils.findMinimumSetOfHints(
                        solvingContext, problemRepository.getGrandProblem(),
                        solvingStrategy, hints);

                // Create and return solution object.
                var initialValues = GridUtils.convertIntegerHintListTo2DArray(hints, width, width);
                return this.solve(new Sudoku(tiling, initialValues));
            }
        }
    }

    public interface Factory {
        SudokuService create();
    }
}
