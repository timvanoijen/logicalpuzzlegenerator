package nl.timvanoijen.logicalpuzzles.sudoku.services;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemProvider;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.problems.providers.UniqueValuesConditionProvider;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.service.GridUtils;
import nl.timvanoijen.logicalpuzzles.sudoku.models.SudokuTiling;

import java.util.ArrayList;
import java.util.List;

public class SudokuProblemProvider implements ProblemProvider {

    private final IntVar.ProviderFactory intProviderFactory;
    private final UniqueValuesConditionProvider.Factory uniqueValuesConditionProviderFactory;
    private final GridKeysGenerator gridKeysGenerator;

    private final Integer[][] values;
    private final int blockWidth;
    private final int nrBlocksHorizontal;
    private final int blockHeight;
    private final int nrBlocksVertical;
    private final int width;
    private final int height;

    @Inject
    public SudokuProblemProvider(UniqueValuesConditionProvider.Factory uniqueValuesConditionProviderFactory,
                                 IntVar.ProviderFactory intProviderFactory,
                                 @Assisted SudokuTiling tiling,
                                 @Assisted Integer[][] values,
                                 @Assisted GridKeysGenerator gridKeysGenerator) {
        this.uniqueValuesConditionProviderFactory = uniqueValuesConditionProviderFactory;
        this.intProviderFactory = intProviderFactory;
        this.values = values;
        this.gridKeysGenerator = gridKeysGenerator;
        this.blockWidth = tiling.getBlockWidth();
        this.nrBlocksHorizontal = tiling.getNrBlocksHorizontal();
        this.blockHeight = tiling.getBlockHeight();
        this.nrBlocksVertical = tiling.getNrBlocksVertical();
        this.width = tiling.getWidth();
        this.height = tiling.getHeight();
    }

    @Override
    public Problem provide(ProblemRepository problemRepository) {

        // Create integer cells
        problemRepository.createFinalVariables(this.gridKeysGenerator.elements(),
            this.intProviderFactory.create().withRange(new IntVar.Range(1, this.width)));

        // Create unique values conditions for all rows and columns.
        for (var row : gridKeysGenerator.rowGroups())
            problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(row));
        for (var column : gridKeysGenerator.columnGroups())
            problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(column));

        // Create unique values for all blocks
        for (int i = 0; i < this.nrBlocksHorizontal; i++) {
            for (int j = 0; j < this.nrBlocksVertical; j++) {
                List<Key> block = new ArrayList<>();
                for (int ii = 0; ii < this.blockWidth; ii++) {
                    for (int jj = 0; jj < this.blockHeight; jj++) {
                        block.add(gridKeysGenerator.element(i * this.blockWidth + ii + 1,
                                j * this.blockHeight + jj + 1));
                    }
                }
                problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(block));
            }
        }

        // Set initial values
        GridUtils.convert2DArrayToGridVariables(this.values,
                this.gridKeysGenerator, problemRepository);

        return problemRepository.getGrandProblem();
    }

    public interface Factory {
        SudokuProblemProvider create(SudokuTiling tiling,
                                     Integer[][] values,
                                     GridKeysGenerator gridKeysGenerator);
    }
}
