package nl.timvanoijen.logicalpuzzles.sudoku.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.drawing.LineStyle;
import nl.timvanoijen.logicalpuzzles.drawing.PuzzleStringBuilder;
import nl.timvanoijen.logicalpuzzles.drawing.Size;
import nl.timvanoijen.logicalpuzzles.drawing.UtfElementDrawer;

@Getter
@AllArgsConstructor
public class Sudoku {
    private final SudokuTiling tiling;
    private final Integer[][] values;

    @Override
    public String toString() {
        var width = this.tiling.getWidth();
        var nrDigits = ((int)Math.log10(width)) + 1;
        var pb = new PuzzleStringBuilder()
                .withCellContentSize(new Size(nrDigits, 1));
        for (int row = 0; row < width; row++) {
            for (int col = 0; col < width; col++) {
                var value = this.values[col][row];
                var stringValue = value == null ? " " : Integer.toString(value);
                pb.addCell(col, row, stringValue);
                pb.addRectangle(col, row, 1, 1, LineStyle.SolidLight);
            }
        }

        for (int i = 0; i < this.tiling.getNrBlocksHorizontal(); i++) {
            for (int j = 0; j < this.tiling.getNrBlocksVertical(); j++) {
                pb.addRectangle(i*this.tiling.getBlockWidth(), j*this.tiling.getBlockHeight(),
                        this.tiling.getBlockWidth(), this.tiling.getBlockHeight(), LineStyle.SolidHeavy);
            }
        }
        return pb.build(new UtfElementDrawer());
    }
}
