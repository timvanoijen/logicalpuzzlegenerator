package nl.timvanoijen.logicalpuzzles.numbersquare.services;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.timvanoijen.logicalpuzzles.numbersquare.NumberSquareDependencyInjectionModule;
import nl.timvanoijen.logicalpuzzles.numbersquare.models.NumberSquare;
import nl.timvanoijen.logicalpuzzles.numbersquare.models.NumberSquareSolution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Objects;

class NumberSquarePuzzleTest {

    @Inject NumberSquareService.Factory numberSquareServiceFactory;

    @Test
    void testSolvePuzzle1() {
        Integer[][] input = {
                { 1, 0, 0, 0, 4 },
                { 3, 0, 1, 0, 2 },
                { 0, 2, 3, 4, 1 },
                { 4, 0, 0, 0, 3 },
                { 2, 0, 4, 1, 5 }
        };

        Integer[][] solution = {
                { 1, 5, 2, 3, 4 },
                { 3, 4, 1, 5, 2 },
                { 5, 2, 3, 4, 1 },
                { 4, 1, 5, 2, 3 },
                { 2, 3, 4, 1, 5 }
        };

        this.doTest(input, solution);
    }

    @Test
    void testGenerate() {
        for(var k =0; k < 20; k++) {

            var width = 9;
            var service = this.numberSquareServiceFactory.create();
            NumberSquareSolution solution1 = service.generate(width);
            var solution2 = service.solve(solution1.getInitialPuzzle());

            for (var i = 0; i < width; i++)
                for (var j = 0; j < width; j++)
                    assert Objects.equals(solution1.getSolvedPuzzle().getValues()[i][j],
                            solution2.getSolvedPuzzle().getValues()[i][j]);
        }
    }

    private void doTest(Integer[][] input, Integer[][] expectedResult) {
        int width = input.length;
        var transformedInput = new Integer[width][width];
        for(var i = 0; i < width; i++)
            for(var j = 0; j < width; j++)
                transformedInput[i][j] = input[i][j] == 0 ? null : input[i][j];

        NumberSquare numberSquare = new NumberSquare(width, transformedInput);

        var service = this.numberSquareServiceFactory.create();

        NumberSquareSolution solution = service.solve(numberSquare);
        Integer[][] solutionSquare = solution.getSolvedPuzzle().getValues();

        for(var i = 0; i < width; i++)
            for(var j = 0; j < width; j++)
                assert Objects.equals(expectedResult[i][j], solutionSquare[i][j]);
    }

    @BeforeEach
    public void init() {
        Injector injector = Guice.createInjector(new NumberSquareDependencyInjectionModule());
        injector.injectMembers(this);
    }
}