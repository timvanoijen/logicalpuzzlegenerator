package nl.timvanoijen.logicalpuzzles.numbersquare.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.models.GridEntry;

import java.util.List;

@Getter
@AllArgsConstructor
public class NumberSquareSolution {
    private final NumberSquare initialPuzzle;
    private final NumberSquare solvedPuzzle;
    private final List<GridEntry> steps;
    private final int difficulty;
}
