package nl.timvanoijen.logicalpuzzles.numbersquare.services;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.numbersquare.models.NumberSquare;
import nl.timvanoijen.logicalpuzzles.service.GridUtils;

public class NumberSquareProblemConverter {

    private final NumberSquareProblemProvider.Factory numberSquareProblemProviderFactory;
    private final GridKeysGenerator gridKeysGenerator;
    private final int width;

    @Inject
    public NumberSquareProblemConverter(NumberSquareProblemProvider.Factory numberSquareProblemProviderFactory,
                                        @Assisted GridKeysGenerator gridKeysGenerator,
                                        @Assisted int width) {
        this.numberSquareProblemProviderFactory = numberSquareProblemProviderFactory;
        this.gridKeysGenerator = gridKeysGenerator;
        this.width = width;
    }

    public NumberSquare toNumberSquare(ProblemRepository problemRepository) {
        var values = GridUtils.convertGridVariablesTo2DArray(
                this.gridKeysGenerator, problemRepository);
        return new NumberSquare(width, values);
    }

    public ProblemRepository toProblemRepository(NumberSquare numberSquare) {
        var problemRepository = new ProblemRepository();

        problemRepository.extend(
            this.numberSquareProblemProviderFactory.create(
                    numberSquare.getValues(),
                    gridKeysGenerator));
        return problemRepository;
    }

    public interface Factory {
        NumberSquareProblemConverter create(GridKeysGenerator gridKeysGenerator, int width);
    }
}
