package nl.timvanoijen.logicalpuzzles.numbersquare.services;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.ColumnProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.OptionProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.KeyBuilder;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;
import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.RowProperty;
import nl.timvanoijen.logicalpuzzles.core.solvers.DirectSolvingStrategy;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingContext;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingResult;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.numbersquare.models.NumberSquare;
import nl.timvanoijen.logicalpuzzles.numbersquare.models.NumberSquareSolution;
import nl.timvanoijen.logicalpuzzles.service.GenerationUtils;
import nl.timvanoijen.logicalpuzzles.service.GridUtils;
import nl.timvanoijen.logicalpuzzles.service.Hint;
import nl.timvanoijen.logicalpuzzles.service.HintList;

import java.util.List;
import java.util.stream.Collectors;

public class NumberSquareService {

    private final SolvingContext.Factory solvingContextFactory;
    private final DirectSolvingStrategy.Factory directSolvingStrategyFactory;
    private final NumberSquareProblemConverter.Factory numberSquareProblemConverterFactory;
    private final NumberSquareProblemProvider.Factory numberSquareProblemProviderFactory;

    @Inject
    public NumberSquareService(SolvingContext.Factory solvingContextFactory,
                               NumberSquareProblemConverter.Factory numberSquareProblemConverterFactory,
                               DirectSolvingStrategy.Factory directSolvingStrategyFactory,
                               NumberSquareProblemProvider.Factory numberSquareProblemProviderFactory) {
        this.solvingContextFactory = solvingContextFactory;
        this.numberSquareProblemConverterFactory = numberSquareProblemConverterFactory;
        this.directSolvingStrategyFactory = directSolvingStrategyFactory;
        this.numberSquareProblemProviderFactory = numberSquareProblemProviderFactory;
    }

    public NumberSquareSolution solve(NumberSquare numberSquare) {

        // Initialize
        var width = numberSquare.getWidth();
        var gridKeysGenerator = new GridKeysGenerator(StringKey.of("cell"), width, width);
        var converter = this.numberSquareProblemConverterFactory
                .create(gridKeysGenerator, width);

        // Solve
        var problemRepository = converter.toProblemRepository(numberSquare);
        var solvingStrategy = this.directSolvingStrategyFactory.create();
        try(var solvingContext = this.solvingContextFactory.create(problemRepository)) {
            SolvingResult solvingResult = solvingContext.solve(
                    problemRepository.getGrandProblem(), solvingStrategy);

            // TODO: create proper result object to indicate that it is not feasible
            // or not solvable.
            if (!solvingResult.isSucceeded())
                return null;
        }

        // Get solution steps
        var steps = GridUtils.convertGridVariablesToSortedGridEntries(
                gridKeysGenerator, problemRepository);

        // Create solution object
        var solutionNumberSquare = converter.toNumberSquare(problemRepository);
        return new NumberSquareSolution(numberSquare, solutionNumberSquare, steps, 0);
    }

    public NumberSquareSolution generate(int width) {
        var gridKeysGenerator = new GridKeysGenerator(StringKey.of("cell"), width, width);

        // Build initial empty problem
        NumberSquare numberSquare = new NumberSquare(width, new Integer[width][width]);
        var converter = this.numberSquareProblemConverterFactory
                .create(gridKeysGenerator, width);
        var problemRepository = converter.toProblemRepository(numberSquare);

        // Select puzzle variables to pick from
        var optionVars = KeyBuilder.forkFrom(gridKeysGenerator.elements())
                .fork(OptionProperty.range(1, width))
                .build().flatten().stream()
                .map(problemRepository::<BoolVar>getVariable)
                .collect(Collectors.toList());

        try(var solvingContext = this.solvingContextFactory.create(problemRepository)) {
            var solvingStrategy = this.directSolvingStrategyFactory.create();

            try(var executionContext = solvingContext.provideExecutionContext()) {
                var noHintsCheckpoint = executionContext.newVersion();

                // Generate puzzle by iteratively adding a new initial value and solving.
                List<BoolVar> initialOptionVars = GenerationUtils.generateBySelectAndSolve(solvingContext,
                        problemRepository, solvingStrategy, optionVars);

                // Translate initial option vars into hints.
                var hints = HintList.fromVars(initialOptionVars.stream()
                        .map(v -> problemRepository.<IntVar>getVariable(gridKeysGenerator.element(
                                v.getKey().get(ColumnProperty.extract()),
                                v.getKey().get(RowProperty.extract())
                        ))).collect(Collectors.toList()));

                // Reduce number of hints
                executionContext.rollbackToVersion(noHintsCheckpoint - 1);
                var reducedHints = GenerationUtils.findMinimumSetOfHints(
                        solvingContext, problemRepository.getGrandProblem(),
                        solvingStrategy, hints);

                // Create and return solution object.
                var initialValues = GridUtils.convertIntegerHintListTo2DArray(reducedHints, width, width);
                return this.solve(new NumberSquare(width, initialValues));
            }
        }
    }

    public interface Factory {
        NumberSquareService create();
    }
}
