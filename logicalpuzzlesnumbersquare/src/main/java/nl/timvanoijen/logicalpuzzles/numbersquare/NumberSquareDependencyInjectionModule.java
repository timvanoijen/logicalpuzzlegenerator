package nl.timvanoijen.logicalpuzzles.numbersquare;

import com.google.inject.assistedinject.FactoryModuleBuilder;
import nl.timvanoijen.logicalpuzzles.core.DefaultDependencyInjectionModule;
import nl.timvanoijen.logicalpuzzles.numbersquare.services.NumberSquareProblemConverter;
import nl.timvanoijen.logicalpuzzles.numbersquare.services.NumberSquareProblemProvider;
import nl.timvanoijen.logicalpuzzles.numbersquare.services.NumberSquareService;


public class NumberSquareDependencyInjectionModule extends DefaultDependencyInjectionModule {
    @Override
    protected void configure() {
        super.configure();

        // Build factories
        install(new FactoryModuleBuilder().build(NumberSquareService.Factory.class));
        install(new FactoryModuleBuilder().build(NumberSquareProblemProvider.Factory.class));
        install(new FactoryModuleBuilder().build(NumberSquareProblemConverter.Factory.class));
    }
}
