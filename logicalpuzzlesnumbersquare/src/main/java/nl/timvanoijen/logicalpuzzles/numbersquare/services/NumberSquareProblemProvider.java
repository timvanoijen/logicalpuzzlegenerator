package nl.timvanoijen.logicalpuzzles.numbersquare.services;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemProvider;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.problems.providers.UniqueValuesConditionProvider;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.service.GridUtils;

public class NumberSquareProblemProvider implements ProblemProvider {

    private final IntVar.ProviderFactory intProviderFactory;
    private final UniqueValuesConditionProvider.Factory uniqueValuesConditionProviderFactory;
    private final int width;
    private final Integer[][] values;
    private final GridKeysGenerator gridKeysGenerator;

    @Inject
    public NumberSquareProblemProvider(UniqueValuesConditionProvider.Factory uniqueValuesConditionProviderFactory,
                              IntVar.ProviderFactory intProviderFactory,
                              @Assisted GridKeysGenerator gridKeysGenerator,
                              @Assisted Integer[][] values) {
        this.uniqueValuesConditionProviderFactory = uniqueValuesConditionProviderFactory;
        this.intProviderFactory = intProviderFactory;
        this.width = values.length;
        this.values = values;
        this.gridKeysGenerator = gridKeysGenerator;
    }

    @Override
    public Problem provide(ProblemRepository problemRepository) {

        // Create integer cells
        problemRepository.createFinalVariables(this.gridKeysGenerator.elements(),
            this.intProviderFactory.create().withRange(new IntVar.Range(1, this.width)));

        // Create unique values conditions for all rows, columns and diagonals.
        for (var row : gridKeysGenerator.rowGroups())
            problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(row));
        for (var column : gridKeysGenerator.columnGroups())
            problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(column));
        problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(gridKeysGenerator.diagonalTopLeftGroup()));
        problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(gridKeysGenerator.diagonalBottomLeftGroup()));

        // Set initial values
        GridUtils.convert2DArrayToGridVariables(this.values,
                this.gridKeysGenerator, problemRepository);

        return problemRepository.getGrandProblem();
    }

    public interface Factory {
        NumberSquareProblemProvider create(Integer[][] values, GridKeysGenerator gridKeysGenerator);
    }
}
