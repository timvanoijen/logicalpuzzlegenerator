package nl.timvanoijen.logicalpuzzles.numbersquare.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.drawing.LineStyle;
import nl.timvanoijen.logicalpuzzles.drawing.PuzzleStringBuilder;
import nl.timvanoijen.logicalpuzzles.drawing.Size;
import nl.timvanoijen.logicalpuzzles.drawing.UtfElementDrawer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class NumberSquare {
    private final int width;
    private final Integer[][] values;

    @Override
    public String toString() {
        var nrDigits = ((int)Math.log10(width)) + 1;
        var pb = new PuzzleStringBuilder()
                .withCellContentSize(new Size(nrDigits, 1));
        for (int row = 0; row < this.width; row++) {
            for (int col = 0; col < this.width; col++) {
                var value = this.values[col][row];
                var stringValue = value == null ? " " : Integer.toString(value);
                pb.addCell(col, row, stringValue);
                pb.addRectangle(col, row, 1, 1, LineStyle.SolidLight);
            }
        }
        pb.addRectangle(0,0, width, width, LineStyle.SolidHeavy);
        return pb.build(new UtfElementDrawer());
    }
}
