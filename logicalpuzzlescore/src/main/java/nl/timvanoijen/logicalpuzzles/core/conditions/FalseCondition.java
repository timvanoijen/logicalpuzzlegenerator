package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.TypeProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.Collections;

public class FalseCondition extends BoolVar implements Condition {

    private final BoolVar var;

    private FalseCondition(Key key, Boolean defaultValue,
                           StateBinding<Boolean> stateBinding, BoolVar var) {
        super(key, defaultValue, stateBinding);

        ConditionUtils.verifyVariableIsFinal(var);
        this.var = var;
    }

    @Override
    public Iterable<Variable> getInvolvedVariables() {
        return Collections.singletonList(this.var);
    }

    @Override
    public boolean processUpdates(Iterable<Variable> changedVariables) {
        Boolean conditionValue = this.getValue();
        Boolean varValue = this.var.getValue();

        if (conditionValue == null) {
            if (varValue != null) {
                this.setValue(!varValue);
                return true;
            }
        } else if (conditionValue == true) {
            if (varValue == null) {
                this.var.setValue(false);
                return true;
            }
            return varValue == false;
        } else if (conditionValue == false) {
            if (varValue == null) {
                this.var.setValue(true);
                return true;
            }
            return varValue == true;
        }
        return true;
    }

    public interface ProviderFactory {
        Provider create(BoolVar boolVar);
    }

    public static class Provider implements ConditionProvider<FalseCondition> {

        private final BoolVar boolVar;
        private Boolean defaultValue = true;

        @Inject
        public Provider(@Assisted BoolVar boolVar) {
            this.boolVar = boolVar;
        }

        public Provider withDefault(Boolean defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        @Override
        public FalseCondition provide(Key key, StateBinding<Boolean> stateBinding,
                ProblemRepository problemRepository) {
            return new FalseCondition(key, defaultValue, stateBinding, this.boolVar);
        }

        @Override
        public Key getDefaultKey() {
            return CompositeKey.create()
                    .with(TypeProperty.of(FalseCondition.class))
                    .with(KeyProperty.of("var", this.boolVar.getKey()));
        }
    }
}
