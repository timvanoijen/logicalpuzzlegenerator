package nl.timvanoijen.logicalpuzzles.core.variables;

import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;


public interface VariableProvider<U, T extends Variable<U>> {
    T provide(Key key, StateBinding<U> stateBinding);
}
