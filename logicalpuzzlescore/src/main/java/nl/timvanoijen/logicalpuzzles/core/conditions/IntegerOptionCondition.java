package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.TypeProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.Arrays;

public class IntegerOptionCondition extends BoolVar implements Condition {

    private final IntVar intVar;
    private final BoolVar boolVar;
    private final int equivalenceValue;

    private IntegerOptionCondition(Key key, Boolean defaultValue,
                                   StateBinding<Boolean> stateBinding,
                                   IntVar intVar, BoolVar boolVar, int equivalenceValue) {
        super(key, defaultValue, stateBinding);

        ConditionUtils.verifyVariableIsFinal(intVar);
        ConditionUtils.verifyVariableIsFinal(boolVar);

        this.intVar = intVar;
        this.boolVar = boolVar;
        this.equivalenceValue = equivalenceValue;
    }

    @Override
    public Iterable<Variable> getInvolvedVariables() {
        return Arrays.asList( this.intVar, this.boolVar );
    }

    @Override
    public boolean processUpdates(Iterable<Variable> changedVariables) {
        ConditionUtils.verifyConditionValueIsTrue(this);

        // Block for 'int is null'
        if (this.intVar.getValue() == null) {
            if (this.boolVar.getValue() == null)
                return true;
            if (this.boolVar.getValue() == true)
                this.intVar.setValue(equivalenceValue);
            return true;
        }

        // Block for 'int has value', 'bool is null'
        if (this.boolVar.getValue() == null) {
            this.boolVar.setValue(this.intVar.getValue() == equivalenceValue);
            return true;
        }

        // Block for 'int has value', 'bool has value'
        return (this.intVar.getValue() == this.equivalenceValue) == this.boolVar.getValue();
    }

    public interface ProviderFactory {
        Provider create(IntVar intVar, BoolVar boolVar, int equivalenceValue);
    }

    public static class Provider implements ConditionProvider<IntegerOptionCondition> {

        private final IntVar intVar;
        private final BoolVar boolVar;
        private final int equivalenceValue;
        private final Boolean defaultValue = true;

        @Inject
        public Provider(@Assisted IntVar intVar, @Assisted BoolVar boolVar,
                        @Assisted int equivalenceValue) {
            this.intVar = intVar;
            this.boolVar = boolVar;
            this.equivalenceValue = equivalenceValue;
        }

        @Override
        public IntegerOptionCondition provide(Key key, StateBinding<Boolean> stateBinding, ProblemRepository problemRepository) {
            return new IntegerOptionCondition(key, defaultValue,
                    stateBinding, this.intVar, this.boolVar, this.equivalenceValue);
        }

        @Override
        public Key getDefaultKey() {
            return CompositeKey.create()
                    .with(TypeProperty.of(IntegerOptionCondition.class))
                    .with(KeyProperty.of("int", this.intVar.getKey()))
                    .with(KeyProperty.of("bool", this.boolVar.getKey()))
                    .with(KeyProperty.of("value", IntKey.of(this.equivalenceValue)));
        }
    }
}