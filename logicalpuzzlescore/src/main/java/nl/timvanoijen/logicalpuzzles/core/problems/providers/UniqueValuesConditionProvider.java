package nl.timvanoijen.logicalpuzzles.core.problems.providers;

import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.conditions.CountTrueCondition;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.OptionProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.keys.KeyBuilder;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemCollector;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemProvider;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;

import java.util.*;

public class UniqueValuesConditionProvider implements ProblemProvider {

    private final Iterable<? extends Key> integerKeys;
    private final CountTrueCondition.ProviderFactory countTrueConditionProviderFactory;
    private final IntegerOptionsProvider.Factory integerOptionsProviderFactory;

    @Inject
    public UniqueValuesConditionProvider(@Assisted Iterable<? extends Key> integerKeys,
                                         CountTrueCondition.ProviderFactory countTrueConditionProviderFactory,
                                         IntegerOptionsProvider.Factory integerOptionsProviderFactory) {
        this.integerKeys = integerKeys;
        this.countTrueConditionProviderFactory = countTrueConditionProviderFactory;
        this.integerOptionsProviderFactory = integerOptionsProviderFactory;
    }

    @Override
    public Problem provide(ProblemRepository problemRepository) {

        Set<Integer> usedOptions = new HashSet<>();

        List<Problem> integerOptionProblems = new ArrayList<>();
        List<Condition> uniqueValueConditions = new ArrayList<>();

        for(Key key : this.integerKeys) {
            IntVar var = problemRepository.getVariable(key);

            // Ensure integer options (and corresponding conditions)
            var p = problemRepository.extend(this.integerOptionsProviderFactory.create(key));
            integerOptionProblems.add(p);

            IntVar.Range rng = var.getRange();
            assert rng != null;
            for(var i = rng.getFrom(); i <= rng.getTill(); i++)
                usedOptions.add(i);
        }

        int from = Collections.min(usedOptions);
        int till = Collections.max(usedOptions);

        // Key iterable grouped by their option value
        var optionLists = KeyBuilder
                .forkFrom(this.integerKeys)
                .fork(OptionProperty.range(from, till))
                .build();

        // If number of options equals the number of integers,
        // we know that each option should be taken exactly once.
        if (usedOptions.size() == Iterables.size(this.integerKeys)) {
            for(var optionList : optionLists) {
                var c = problemRepository.getOrCreateCondition(
                        this.countTrueConditionProviderFactory
                                .create(problemRepository.getVariables(optionList))
                                .withMinTrue(1).withMaxTrue(1));
                uniqueValueConditions.add(c);
            }
        } else if (usedOptions.size() > Iterables.size(this.integerKeys)) {
            // If number of options is larger than number of integers,
            // we know that each option should be taken AT MAX once.
            for(var optionList : optionLists) {

                var c = problemRepository.getOrCreateCondition(
                        this.countTrueConditionProviderFactory
                                .create(problemRepository.getVariables(optionList))
                                .withMinTrue(0).withMaxTrue(1));
                uniqueValueConditions.add(c);
            }
        } else
            throw new IllegalStateException("Number of options cannot be less than number of unique variables.");

        // Return "Integer with options"-problems together with the uniqueness
        // conditions as sub-problem.
        return new ProblemCollector()
                .add(integerOptionProblems)
                .addConditions(uniqueValueConditions)
                .getProblem();
    }

    public interface Factory {
        UniqueValuesConditionProvider create(Iterable<? extends Key> integerKey);
    }
}