package nl.timvanoijen.logicalpuzzles.core.state;

public interface ExecutionContextProvider {
    ExecutionContext provideExecutionContext();
}
