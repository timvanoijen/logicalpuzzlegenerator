package nl.timvanoijen.logicalpuzzles.core.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Memoization {
    public static <P,R> Function<P,R> memoize(Function<P,R> unMemoized) {
        Map<P, R> cache = new HashMap<>();
        return (P p) -> cache.computeIfAbsent(p, unMemoized);
    }
}
