package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import nl.timvanoijen.logicalpuzzles.core.keys.Key;

public interface KeyPropertyExtractor<T> {
    T extract(Key key);
}
