package nl.timvanoijen.logicalpuzzles.core.puzzle;

import nl.timvanoijen.logicalpuzzles.core.problems.Problem;

public class SimplePuzzle implements Puzzle {

    private final Problem problem;

    public static Puzzle fromProblem(Problem problem) {
        return new SimplePuzzle(problem);
    }

    private SimplePuzzle(Problem problem) {
        this.problem = problem;
    }

    @Override
    public Problem getProblem() {
        return this.problem;
    }
}
