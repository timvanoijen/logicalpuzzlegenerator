package nl.timvanoijen.logicalpuzzles.core.problems;

import com.google.common.collect.Streams;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.conditions.ConditionProvider;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.state.*;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;
import nl.timvanoijen.logicalpuzzles.core.variables.VariableProvider;

import java.util.*;
import java.util.stream.Collectors;

public class ProblemRepository implements ExecutionContextProvider {

    private final Repository repository;
    private final SinkRepository sinkRepository;
    private final Map<String, Variable> variableCache = new HashMap<>();
    private final Map<String, Condition> conditionCache = new HashMap<>();

    public ProblemRepository() {
        this.sinkRepository = new SinkRepository();
        this.repository = new ProxyRepository(this.sinkRepository);
    }

    // --------------------------------------------------
    // -- Execution context
    // --------------------------------------------------

    public ExecutionContext provideExecutionContext() {
        return new ExecutionContext(this.sinkRepository);
    }

    // --------------------------------------------------
    // -- Problems
    // --------------------------------------------------

    public Problem getGrandProblem() {
        return new Problem(this.getVariables(), this.getConditions());
    }

    public Problem extend(ProblemProvider provider) {
        var result = provider.provide(this);

        // Verify that all returned variables have been registered.
        result.getVariables().forEach(v -> {
            if(!this.variableCache.containsKey(v.getKey().asString()))
                throw new IllegalArgumentException("ProblemProvider returned unregistered variable.");
        });

        // Verify that all returned conditions have been registered.
        result.getConditions().forEach(v -> {
            if(!this.conditionCache.containsKey(v.getKey().asString()))
                throw new IllegalArgumentException("ProblemProvider returned unregistered condition.");
        });

        return result;
    }

    // --------------------------------------------------
    // -- Variables
    // --------------------------------------------------

    // ------ Variables: CREATE ------

    public <U, T extends Variable<U>> T createVariable(Key key,
            VariableProvider<U,T> provider) {
        verifyKeyAbsent(key, this.variableCache);
        return this.doCreateVariable(key, provider, false);
    }

    public <U, T extends Variable<U>> T createFinalVariable(Key key,
            VariableProvider<U,T> provider) {
        verifyKeyAbsent(key, this.variableCache);
        return this.doCreateVariable(key, provider, true);
    }

    public <U, T extends Variable<U>> List<T> createVariables(
            Iterable<? extends Key> keys, VariableProvider<U,T> provider) {
        return Streams.stream(keys)
                .map(k -> this.createVariable(k, provider))
                .collect(Collectors.toList());
    }

    public <U, T extends Variable<U>> List<T> createFinalVariables(
            Iterable<? extends Key> keys, VariableProvider<U,T> provider) {
        return Streams.stream(keys)
                .map(k -> this.createFinalVariable(k, provider))
                .collect(Collectors.toList());
    }

    // ------ Variables: GET ------

    public <T extends Variable> T getVariable(Key key) {
        verifyKeyPresent(key, this.variableCache);
        return getFromCacheAndCheckForFinal(key, this.variableCache, null);
    }

    public <T extends Variable> List<T> getVariables(
            Iterable<? extends Key> keys) {
        return Streams.stream(keys)
                .map(this::<T>getVariable)
                .collect(Collectors.toList());
    }

    public List<Variable> getVariables() {
        return new ArrayList<>(this.variableCache.values());
    }

    // ------ Variables: GET OR CREATE ------

    public <U, T extends Variable<U>> T getOrCreateVariable(Key key,
            VariableProvider<U,T> provider) {
        T result = getFromCacheAndCheckForFinal(key, this.variableCache, false);
        return (result != null) ? result : this.createVariable(key, provider);
    }

    public <U, T extends Variable<U>> T getOrCreateFinalVariable(Key key,
            VariableProvider<U,T> provider) {
        T result = getFromCacheAndCheckForFinal(key, this.variableCache, true);
        return (result != null) ? result : this.createFinalVariable(key, provider);
    }

    public <U, T extends Variable<U>> List<T> getOrCreateVariables(
            Iterable<? extends Key> keys, VariableProvider<U,T> provider) {
        return Streams.stream(keys)
                .map(k -> this.getOrCreateVariable(k, provider))
                .collect(Collectors.toList());
    }

    public <U, T extends Variable<U>> List<T> getOrCreateFinalVariables(
            Iterable<? extends Key> keys, VariableProvider<U,T> provider) {
        return Streams.stream(keys)
                .map(k -> this.getOrCreateFinalVariable(k, provider))
                .collect(Collectors.toList());
    }

    // --------------------------------------------------
    // -- Conditions
    // --------------------------------------------------

    // ------ Conditions: CREATE ------

    public <T extends Condition> T createCondition(Key key,
                ConditionProvider<T> provider) {
        verifyKeyAbsent(key, this.conditionCache);
        return this.doCreateCondition(key, provider);
    }

    public <T extends Condition> T createCondition(ConditionProvider<T> provider) {
        return this.createCondition(provider.getDefaultKey(), provider);
    }

    // ------ Conditions: GET ------

    public <T extends Condition> T getCondition(Key key) {
        verifyKeyPresent(key, this.conditionCache);
        return getFromCacheAndCheckForFinal(key, this.conditionCache, true);
    }

    public <T extends Condition> List<T> getConditions(
            Iterable<? extends Key> keys) {
        return Streams.stream(keys)
                .map(this::<T>getCondition)
                .collect(Collectors.toList());
    }

    public List<Condition> getConditions() {
        return new ArrayList<>(this.conditionCache.values());
    }

    // ------ Conditions: GET OR CREATE ------

    public <T extends Condition> T getOrCreateCondition(Key key,
                                                             ConditionProvider<T> provider) {
        T result = getFromCacheAndCheckForFinal(key, this.conditionCache, true);
        return (result != null) ? result : this.createCondition(key, provider);
    }

    public <T extends Condition> T getOrCreateCondition(ConditionProvider<T> provider) {
        return this.getOrCreateCondition(provider.getDefaultKey(), provider);
    }

    // Private methods

    private static void verifyKeyAbsent(Key key, Map<String,?> map) {
        if (map.containsKey(key.asString()))
            throw new IllegalStateException(
                    String.format("Key %s already exists", key.asString()));
    }

    private static void verifyKeyPresent(Key key, Map<String,?> map) {
        if (!map.containsKey(key.asString()))
            throw new IllegalStateException(
                    String.format("Key %s does not exist", key.asString()));
    }

    private <U, T extends Variable<U>> T doCreateVariable(Key key,
                VariableProvider<U,T> provider, Boolean isFinal) {

        int id = this.repository.createStateVar(isFinal);
        T result = provider.provide(key, this.repository.getBinding(id));
        this.variableCache.put(key.asString(), result);
        return result;
    }

    private <T extends Condition> T doCreateCondition(Key key,
            ConditionProvider<T> provider) {

        int id = this.repository.createStateVar(true);
        T result = provider.provide(key, this.repository.getBinding(id), this);
        this.conditionCache.put(key.asString(), result);
        return result;
    }

    @SuppressWarnings("unchecked")
    private static <T extends Variable> T getFromCacheAndCheckForFinal(Key key,
                                                                       Map<String, ? extends Variable> cache, Boolean isFinal) {

        String keyString = key.asString();
        if (cache.containsKey(keyString)) {
            T result;
            try {
                result = (T)cache.get(keyString);
                assert isFinal == null || isFinal == result.isFinal();
                return result;
            } catch(Exception e) {
                throw new IllegalArgumentException(String.format(
                        "Variable or condition %s does not have requested type or reassignment policy",
                        keyString ));
            }
        }

        return null;
    }
}
