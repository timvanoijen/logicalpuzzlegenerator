package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.TypeProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;

import java.util.Collection;

public class AndCondition extends CountTrueCondition {

    private AndCondition(Key key, Boolean defaultValue,
                         StateBinding<Boolean> stateBinding,
                         Collection<BoolVar> vars,
                         ProblemRepository problemRepository,
                         IntVar.ProviderFactory intProviderFactory) {
        super(key, defaultValue, stateBinding, vars, vars.size(), vars.size(),
                problemRepository, intProviderFactory);
    }

    public interface ProviderFactory {
        Provider create(Collection<BoolVar> boolVars);
    }

    public static class Provider implements ConditionProvider<AndCondition> {

        private final IntVar.ProviderFactory intVarProviderFactory;

        private final Collection<BoolVar> boolVars;
        private Boolean defaultValue = true;

        @Inject
        public Provider(@Assisted Collection<BoolVar> boolVars,
                        IntVar.ProviderFactory intVarProviderFactory) {
            this.boolVars = boolVars;
            this.intVarProviderFactory = intVarProviderFactory;
        }

        public AndCondition.Provider withDefault(Boolean defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        @Override
        public AndCondition provide(Key key, StateBinding<Boolean> stateBinding, ProblemRepository problemRepository) {
            return new AndCondition(key, defaultValue, stateBinding,
                    this.boolVars, problemRepository, this.intVarProviderFactory);
        }

        @Override
        public Key getDefaultKey() {
            return CompositeKey.create()
                    .with(TypeProperty.of(AndCondition.class))
                    .with(KeyProperty.of("vars", StringKey.joinKeyProviders(this.boolVars)));
        }
    }
}