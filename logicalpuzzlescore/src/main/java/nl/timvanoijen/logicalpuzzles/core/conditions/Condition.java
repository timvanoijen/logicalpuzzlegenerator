package nl.timvanoijen.logicalpuzzles.core.conditions;

import nl.timvanoijen.logicalpuzzles.core.variables.Variable;


public interface Condition extends Variable<Boolean> {
    Iterable<Variable> getInvolvedVariables();
    boolean processUpdates(Iterable<Variable> changedVariables);
    //TODO: add method to compare two conditions on actual equality
    // (regardless of the key). This method should then be used to
    // identify undesired scenarios where the same conditional logic is
    // registered twice under a different key. We could choose to let
    // conditions auto-generate their key, purely determined by their
    // dependent variables and parameters. However, this would trouble
    // the process of querying conditions in a later stage.
}
