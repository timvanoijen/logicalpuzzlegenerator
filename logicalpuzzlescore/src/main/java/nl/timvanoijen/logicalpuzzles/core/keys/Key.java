package nl.timvanoijen.logicalpuzzles.core.keys;

import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyPropertyExtractor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface Key {

    String getValue();

    default <T> T get(KeyPropertyExtractor<T> extractor) {
        return extractor.extract(this);
    }

    default <T extends Key> T get(String propertyName) {
        throw new KeyException("Key has no properties");
    }

    default Collection<KeyProperty> getProperties() {
        return Collections.emptyList();
    }

    default String asString() {
        List<String> parts = new ArrayList<>();
        if (this.getValue() != null)
            parts.add(this.getValue());
        if (!this.getProperties().isEmpty()) {
            parts.add(String.format("{%s}",
                    this.getProperties().stream()
                            .map(p -> p.getName())
                            .sorted()
                            .map(p -> String.format("{%s=%s}", p, this.get(p).asString()))
                            .collect(Collectors.joining("_&_"))));
        }
        if (parts.size() == 1)
            return parts.get(0);
        if (parts.size() == 2)
            return String.format("{%s:%s}", parts.get(0), parts.get(1));
        return "";
    }

    class KeyException extends RuntimeException {
        public KeyException(String msg) {
            super(msg);
        }
    }
}
