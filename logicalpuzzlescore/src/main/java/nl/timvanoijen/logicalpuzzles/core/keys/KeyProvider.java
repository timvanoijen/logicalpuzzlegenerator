package nl.timvanoijen.logicalpuzzles.core.keys;

public interface KeyProvider {
    Key getKey();
}
