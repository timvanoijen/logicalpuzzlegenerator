package nl.timvanoijen.logicalpuzzles.core.state;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface Repository {

    // Primitive types
    void setStateVarValue(int id, Object val) throws StateReassignedException;
    Object getStateVarValue(int id);
    long getStateVarLastModified(int id);
    boolean isFinal(int id);
    int createStateVar(boolean isFinal);
    void observe(int id, BiConsumer<Integer,Object> consumer);
    void unobserve(int id, BiConsumer<Integer, Object> consumer);

    // Versioning
    int newVersion();
    void rollbackToVersion(int version);

    default <T> StateBinding<T> getBinding(int id) {
        return new SimpleStateBinding<>(id, this);
    }

    class SimpleStateBinding<T> implements StateBinding<T> {
        private final int id;
        private final Repository repository;
        private final Map<Consumer<T>, BiConsumer<Integer, Object>>
                observerAdapters = new HashMap<>();

        SimpleStateBinding(int id, Repository repository) {
            this.id = id;
            this.repository = repository;
        }

        @Override
        public int getId() { return this.id; }

        public T getValue() { return checkedCast(this.repository.getStateVarValue(id), id); }

        public void setValue(T value) { this.repository.setStateVarValue(id, value); }

        public boolean isFinal() { return this.repository.isFinal(id); }

        public long getLastModified() { return this.repository.getStateVarLastModified(id); }

        public void observe(Consumer<T> observer) {
            BiConsumer<Integer, Object> adapter = (i,v) -> observer.accept(checkedCast(v, i));
            this.observerAdapters.put(observer, adapter);
            this.repository.observe(id, adapter);
        }

        public void unobserve(Consumer<T> observer) {
            var adapter = this.observerAdapters.get(observer);
            assert adapter != null;
            this.repository.unobserve(id, adapter);
            this.observerAdapters.remove(observer);
        }

        @SuppressWarnings("unchecked")
        private static <T> T checkedCast(Object o, int id) {
            try {
                return (T)o;
            } catch(ClassCastException ex) {
                throw new IllegalArgumentException(String.format(
                    "State variable with id %d does not have requested type", id));
            }
        }
    }
}
