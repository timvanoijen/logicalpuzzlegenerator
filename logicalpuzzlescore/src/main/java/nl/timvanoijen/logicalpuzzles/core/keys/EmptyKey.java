package nl.timvanoijen.logicalpuzzles.core.keys;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import javax.annotation.concurrent.Immutable;


@Immutable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmptyKey implements Key {

    @Override
    public String getValue() {
        return "";
    }

    public static EmptyKey create() {
        return new EmptyKey();
    }
}
