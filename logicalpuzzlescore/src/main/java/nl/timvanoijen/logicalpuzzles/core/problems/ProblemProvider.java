package nl.timvanoijen.logicalpuzzles.core.problems;

public interface ProblemProvider {
    Problem provide(ProblemRepository problemRepository);
}
