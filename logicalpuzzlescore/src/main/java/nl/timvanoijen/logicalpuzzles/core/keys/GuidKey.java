package nl.timvanoijen.logicalpuzzles.core.keys;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import javax.annotation.concurrent.Immutable;
import java.util.UUID;

@Immutable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GuidKey implements Key {

    private final String value;

    @Override
    public String getValue() {
        return this.value;
    }

    public static GuidKey create() {
        return new GuidKey(UUID.randomUUID().toString());
    }
}
