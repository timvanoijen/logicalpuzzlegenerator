package nl.timvanoijen.logicalpuzzles.core.variables;

import com.google.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;


public class IntVar extends StateBoundVariable<Integer> {

    @Getter
    private final Range range;

    private IntVar(Key key, Range range,
                   Integer defaultValue, StateBinding<Integer> stateBinding) {
        super(key, defaultValue, stateBinding);
        this.range = range;
    }

    @Getter @AllArgsConstructor
    public static class Range {
        private final int from;
        private final int till;
    }

    public interface ProviderFactory {
        Provider create();
    }

    public static class Provider implements VariableProvider<Integer, IntVar> {

        private Range range;
        private Integer defaultValue = null;

        @Inject
        private Provider() {}

        public Provider withRange(Range range) {
            this.range = range;
            return this;
        }

        public Provider withDefault(Integer defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        @Override
        public IntVar provide(Key key, StateBinding<Integer> stateBinding) {
            return new IntVar(key, this.range,
                    this.defaultValue, stateBinding);
        }
    }
}

