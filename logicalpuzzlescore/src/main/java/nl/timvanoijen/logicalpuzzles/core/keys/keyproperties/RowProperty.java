package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import nl.timvanoijen.logicalpuzzles.core.keys.IntKey;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;

import javax.annotation.concurrent.Immutable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Immutable
public class RowProperty extends KeyProperty {

    private final static String name = "row";

    public static RowProperty of(int col) {
        return new RowProperty(col);
    }

    public static Iterable<KeyProperty> range(int from, int till) {
        return IntStream.range(from, till + 1).mapToObj(RowProperty::of)
                .collect(Collectors.toList());
    }

    public static Extractor extract() { return new Extractor(); }

    private RowProperty(int row) {
        super(name, IntKey.of(row));
    }

    public static class Extractor implements KeyPropertyExtractor<Integer> {
        @Override
        public Integer extract(Key key) {
            return Integer.parseInt(key.get(name).getValue());
        }
    }
}

