package nl.timvanoijen.logicalpuzzles.core.variables;

import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;

import java.util.function.Consumer;


public class StateBoundVariable<T> implements Variable<T> {

    private final Key key;
    private final StateBinding<T> stateBinding;

    StateBoundVariable(Key key, T defaultValue, StateBinding<T> stateBinding) {
        this.key = key;
        this.stateBinding = stateBinding;
        this.setValue(defaultValue);
    }

    @Override
    public boolean isFinal() { return this.stateBinding.isFinal(); }

    @Override
    public Key getKey() {
        return this.key;
    }

    @Override
    public int getId() {
        return this.stateBinding.getId();
    }

    @Override
    public T getValue() {
        return this.stateBinding.getValue();
    }

    @Override
    public void setValue(T stateValue) { this.stateBinding.setValue(stateValue); }

    @Override
    public long getLastModified() { return this.stateBinding.getLastModified(); }

    @Override
    public void observe(Consumer<T> observer) {
        this.stateBinding.observe(observer);
    }

    @Override
    public void unobserve(Consumer<T> observer) {
        this.stateBinding.unobserve(observer);
    }

    @Override
    public String toString() {
        return this.key.asString() + "=" +
                (this.getValue() != null ? this.getValue().toString() : "null");
    }
}
