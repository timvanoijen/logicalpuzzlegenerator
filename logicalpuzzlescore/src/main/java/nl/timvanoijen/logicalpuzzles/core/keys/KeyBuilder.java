package nl.timvanoijen.logicalpuzzles.core.keys;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;

import java.util.*;

/**
 * Class to fluently build keys and arbitrary deep nested lists
 * of keys (used for iteration).
 */
public class KeyBuilder<T> {

    private final T keySource;

    public interface KeyIterableSource<T>
            extends List<T> {
        List<Key> flatten();
    }


    /**
     * Creates a KeyBuilder that is initialized with an empty key.
     * @return
     */
    public static KeyBuilder<Key> create() {
        var src = new SingleKeySourceImpl(EmptyKey.create());
        return new KeyBuilder<>(src);
    }

    /**
     * Creates a KeyBuilder that is initialized with the supplied key.
     * @param initialKey
     * @return
     */
    public static KeyBuilder<Key> from(Key initialKey) {
        var src = new SingleKeySourceImpl(initialKey);
        return new KeyBuilder<>(src);
    }

    /**
     * Creates a KeyBuilder that is initialized with the supplied
     * iterable of keys.
     * @param initialKeys
     * @return
     */
    public static KeyBuilder<KeyIterableSource<Key>> forkFrom(
            Iterable<? extends Key> initialKeys) {

        List<SingleKeySourceImpl> singleKeySources = new ArrayList<>();
        for(var key : initialKeys)
            singleKeySources.add(new SingleKeySourceImpl(key));

        // Cast: internal type KeyIterableSourceImpl<SingleKeySourceImpl> to
        //       external type KeyIterableSource<Key>
        return new KeyBuilder<>(uncheckedCast(
                new KeyIterableSourceImpl<>(singleKeySources)));
    }

    private KeyBuilder(T keySource) {
        this.keySource = keySource;
    }

    /**
     * Builds and returns the key or the arbitrarily deeply nested list of keys.
     * @return a key or a List<List<List<...<...>> of composite keys.
     */
    public T build() {
        return this.keySource;
    }

    /**
     * Extend the current key (or key list) with a key property.
     * @param property property to be added to the current key (list).
     * @return         a KeyBuilder instance to continue building.
     */
    public KeyBuilder<T> with(KeyProperty property) {
        // Cast keySource: external type T to
        //                 internal type ExtendableKeySource.
        KeyBuilder.<ExtendableKeySource>uncheckedCast(this.keySource)
                .addProperty(property);
        return this;
    }

    /**
     * Fork and extend the current key (or key list) with an iterable of properties.
     * The result is a new list of keys (or key lists), whose elements
     * are built by extending the original key (or key list) with the
     * different elements in the provided key properties iterable.
     * @param properties iterable of key properties to be added to the current key (list)
     * @return           a KeyBuilder instance to continue building.
     */
    public KeyBuilder<KeyIterableSource<T>> fork(Iterable<? extends KeyProperty> properties) {
        // Cast keySource: external type T to
        //                 internal type ExtendableKeySource.
        ExtendableKeySource keySourceExtendable = uncheckedCast(this.keySource);

        List<ExtendableKeySource> newSources = new ArrayList<>();
        for(var property : properties) {
            ExtendableKeySource newSource = keySourceExtendable;
            newSource = newSource.clone();
            newSource.addProperty(property);
            newSources.add(newSource);
        }

        // Cast result: internal type KeyIterableSourceImpl<ExtendableKeySource> to
        //              external type KeyIterableSource<T>
        return new KeyBuilder<>(uncheckedCast(new KeyIterableSourceImpl<>(newSources)));
    }

    // ***************************************
    // Private interface and implementation classes that allow for
    // extending and forking.
    // ***************************************

    private interface ExtendableKeySource {
        void addProperty(KeyProperty property);
        List<Key> flatten();
        ExtendableKeySource clone();
    }

    private static class SingleKeySourceImpl extends CompositeKey implements ExtendableKeySource {

        public SingleKeySourceImpl(Key key) {
            super(key);
        }

        @Override
        public List<Key> flatten() {
            return Lists.newArrayList(this);
        }

        @SuppressWarnings("MethodDoesntCallSuperMethod")
        @Override
        public SingleKeySourceImpl clone() {
            return new SingleKeySourceImpl(this);
        }

        @Override
        public void addProperty(KeyProperty property) {
            super.addProperty(property);
        }
    }

    @AllArgsConstructor
    private static class KeyIterableSourceImpl<T extends ExtendableKeySource>
            extends AbstractList<T> implements KeyIterableSource<T>, ExtendableKeySource {

        // Wrapped to make unmodifiable by extending AbstractList.
        private final List<T> keySources;

        @Override
        public T get(int index) {
            return this.keySources.get(index);
        }

        @Override
        public int size() {
            return this.keySources.size();
        }

        @Override
        public void addProperty(KeyProperty property) {
            for(ExtendableKeySource src : this.keySources)
                src.addProperty(property);
        }

        @SuppressWarnings("MethodDoesntCallSuperMethod")
        @Override
        public KeyIterableSourceImpl<T> clone() {
            List<T> result = new ArrayList<>();
            for(T s : this.keySources)
                // Cast clone result to concrete type T.
                result.add(uncheckedCast(s.clone()));

            return new KeyIterableSourceImpl<>(result);
        }

        @Override
        public List<Key> flatten() {
            List<Key> result = new ArrayList<>();
            for(ExtendableKeySource src : this.keySources)
                result.addAll(Lists.newArrayList(src.flatten()));
            return result;
        }
    }

    // All unchecked casts (for casting externally exposed types to
    // internally used types and vice versa), are routed through this
    // method.
    @SuppressWarnings("unchecked")
    private static <T> T uncheckedCast(Object o) {
        return (T) o;
    }
}