package nl.timvanoijen.logicalpuzzles.core.problems;

import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class ProblemCollector {

    private Problem problem;

    public ProblemCollector() {
        this.problem = new Problem(Collections.emptyList(), Collections.emptyList());
    }

    public ProblemCollector addVariable(Variable variable) {
        return this.addVariables(Collections.singleton(variable));
    }

    public ProblemCollector addVariables(Iterable<? extends Variable> variables) {
        Iterable<Variable> mergedVariables = Stream.concat(
                StreamSupport.stream(this.problem.getVariables().spliterator(), false),
                StreamSupport.stream(variables.spliterator(), false))
                .distinct().collect(Collectors.toList());
        this.problem = new Problem(mergedVariables, this.problem.getConditions());
        return this;
    }

    public ProblemCollector addCondition(Condition condition) {
        return this.addConditions(Collections.singleton(condition));
    }

    public ProblemCollector addConditions(Iterable<? extends Condition> conditions) {
        Iterable<Condition> mergedConditions = Stream.concat(
                StreamSupport.stream(this.problem.getConditions().spliterator(), false),
                StreamSupport.stream(conditions.spliterator(), false))
                .distinct().collect(Collectors.toList());
        this.problem = new Problem(this.problem.getVariables(), mergedConditions);
        return this;
    }

    public ProblemCollector add(Problem other) {
        return this.addVariables(other.getVariables())
                .addConditions(other.getConditions());
    }

    public ProblemCollector add(Iterable<Problem> others) {
        for(Problem other : others) {
            this.add(other);
        }
        return this;
    }

    public Problem getProblem() {
        return this.problem;
    }
}
