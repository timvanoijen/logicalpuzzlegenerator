package nl.timvanoijen.logicalpuzzles.core.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.*;
import java.util.function.BiConsumer;

public class SinkRepository implements Repository {

    private final Map<Integer, Boolean> stateIdToIsFinalMap = new HashMap<>();
    private final Map<Integer, ValueWithTimestamp> stateVars = new HashMap<>();
    private final Stack<Map<Integer, ValueWithTimestamp>> stateVarCheckpoints = new Stack<>();
    private final Map<Integer, List<BiConsumer<Integer,Object>>> observers = new HashMap<>();

    private long timestamp = 0; // TODO: make int and set timestamps to previous version when rolling back.

    @Override
    public int newVersion() {
        this.stateVarCheckpoints.push(new HashMap<>());
        return this.getCurrentVersion();
    }

    @Override
    public void rollbackToVersion(int version) {
        assert version <= this.getCurrentVersion() ;
        assert version >= 0;

        while(this.stateVarCheckpoints.size() > version) {
            Map<Integer, ValueWithTimestamp> checkpoint = this.stateVarCheckpoints.pop();
            this.stateVars.putAll(checkpoint);
        }
    }

    @Override
    public int createStateVar(boolean isFinal) {
        assert this.getCurrentVersion() == 0;

        int id = stateVars.size() + 1;
        stateIdToIsFinalMap.put(id, isFinal);
        stateVars.put(id, null);
        return id;
    }

    @Override
    public void setStateVarValue(int id, Object val) {

        ValueWithTimestamp origValueWithTimestamp = this.stateVars.get(id);
        Object origValue = origValueWithTimestamp == null ? null : origValueWithTimestamp.getValue();
        if (Objects.equals(origValue, val))
            return;

        if (this.stateIdToIsFinalMap.get(id)
            && origValue != null)
            throw new StateReassignedException();


        if (this.getCurrentVersion() > 0)
            this.stateVarCheckpoints.peek().putIfAbsent(id, origValueWithTimestamp);
        this.stateVars.put(id, new ValueWithTimestamp(val, ++timestamp));

        List<BiConsumer<Integer,Object>> observers = this.observers.get(id);
        if (observers != null)
            for(BiConsumer<Integer, Object> observer: observers)
                observer.accept(id, val);
    }

    @Override
    public Object getStateVarValue(int id) {
        var valueWithTimestamp = this.stateVars.get(id);
        return valueWithTimestamp != null ? valueWithTimestamp.getValue() : null;
    }

    @Override
    public long getStateVarLastModified(int id) {
        var valueWithTimestamp = this.stateVars.get(id);
        return valueWithTimestamp != null ? valueWithTimestamp.getTimestamp() : 0;
    }

    @Override
    public boolean isFinal(int id) {
        return this.stateIdToIsFinalMap.get(id);
    }

    @Override
    public void observe(int id, BiConsumer<Integer, Object> consumer) {
        this.observers.computeIfAbsent(id, k -> new ArrayList<>()).add(consumer);
    }

    @Override
    public void unobserve(int id, BiConsumer<Integer, Object> consumer) {
        this.observers.get(id).remove(consumer);
    }

    private int getCurrentVersion() {
        return stateVarCheckpoints.size();
    }

    @Getter
    @AllArgsConstructor
    private class ValueWithTimestamp {
        private final Object value;
        private final long timestamp;
    }
}
