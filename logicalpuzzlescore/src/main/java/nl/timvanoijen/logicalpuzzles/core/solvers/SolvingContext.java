package nl.timvanoijen.logicalpuzzles.core.solvers;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.ExecutionContext;
import nl.timvanoijen.logicalpuzzles.core.state.ExecutionContextProvider;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.*;
import java.util.function.Consumer;

public class SolvingContext implements ExecutionContextProvider, AutoCloseable {

    private final ProblemRepository problemRepository;
    private final Random random;
    private final Map<Integer, Condition> allConditionsById = new HashMap<>();
    private final Map<Integer, Variable> allVariablesById = new HashMap<>();
    private final Map<Condition, List<Variable>> conditionVariableChanges = new HashMap<>();
    private final Map<Variable, List<Consumer>> variableObservers = new HashMap<>();

    @AssistedInject
    private SolvingContext(@Assisted ProblemRepository problemRepository,
                           @Assisted Random random) {
        this.problemRepository = problemRepository;
        this.random = random;
        this.setup();
    }

    @AssistedInject
    private SolvingContext(@Assisted ProblemRepository problemRepository) {
        this(problemRepository, new Random());
    }

    public Random getRandom() {
        return this.random;
    }

    public SolvingResult solve(Problem problem, SolvingStrategy strategy) {
        var result = strategy.solve(problem, this);

        // To allow for chained or nested solving, we enforce that
        // ALL condition variable changes have been processed by the
        // solve action.
        this.verifyNoConditionVariableChangesToBeProcessed();
        return result;
    }

    @Override
    public ExecutionContext provideExecutionContext() {
        return this.problemRepository.provideExecutionContext();
    }

    @Override
    public void close() {
        this.verifyNoConditionVariableChangesToBeProcessed();
        this.unregisterObservers();
        this.allConditionsById.clear();
        this.allVariablesById.clear();
    }

    Map.Entry<Condition, List<Variable>> pollConditionVariableChanges() {
        var iterator = this.conditionVariableChanges.entrySet().iterator();
        if (!iterator.hasNext())
            return null;
        var result = iterator.next();
        this.conditionVariableChanges.remove(result.getKey());
        return result;
    }

    private void setup() {
        this.problemRepository.getConditions()
                .forEach(c -> this.allConditionsById.put(c.getId(), c));
        this.problemRepository.getVariables()
                .forEach(v -> this.allVariablesById.put(v.getId(), v));
        this.registerObservers();
    }

    private void registerObservers() {
        for(Condition condition : this.allConditionsById.values()) {
            for(Variable<?> variable : condition.getInvolvedVariables()) {

                // In case variable has a value, make sure that conditions are
                // evaluated initially.
                if (variable.getValue() != null)
                    this.registerConditionVariableChange(condition, variable);

                Consumer observer = v ->
                        this.registerConditionVariableChange(condition, variable);
                this.variableObservers.computeIfAbsent(variable, v -> new ArrayList<>())
                        .add(observer);
                variable.observe(observer);
            }
        }
    }

    private void registerConditionVariableChange(Condition condition, Variable variable) {
        this.conditionVariableChanges.computeIfAbsent(condition,
                c -> new ArrayList<>()).add(variable);
    }

    private void unregisterObservers() {
        for(var variable : this.variableObservers.keySet())
            for(var observer : variableObservers.get(variable))
                variable.unobserve(observer);
        this.variableObservers.clear();
    }

    private void verifyNoConditionVariableChangesToBeProcessed() {
        if (!this.conditionVariableChanges.isEmpty())
            throw new SolvingException("There are variable changes that " +
                    "still need to be processed.");
    }

    public static class SolvingException extends RuntimeException {
        public SolvingException(String message) {
            super(message);
        }
    }

    public interface Factory {
        SolvingContext create(ProblemRepository problemRepository);
        SolvingContext create(ProblemRepository problemRepository,
                              Random random);
    }
}
