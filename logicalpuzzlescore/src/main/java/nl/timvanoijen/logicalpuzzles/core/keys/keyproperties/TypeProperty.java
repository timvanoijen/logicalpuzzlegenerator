package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;

import javax.annotation.concurrent.Immutable;
import java.lang.reflect.Type;

@Immutable
public class TypeProperty extends KeyProperty {

    private final static String name = "type";

    public static TypeProperty of(Type type) {
        return new TypeProperty(type);
    }

    public static Extractor extract() { return new Extractor(); }

    private TypeProperty(Type type) {
        super(name, StringKey.of(type.getTypeName()));
    }

    public static class Extractor implements KeyPropertyExtractor<String> {
        @Override
        public String extract(Key key) {
            return key.get(name).getValue();
        }
    }
}

