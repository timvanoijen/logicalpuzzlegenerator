package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.TypeProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;

import java.util.Collection;

public class XOrCondition extends CountTrueCondition {

    private XOrCondition(Key key, Boolean defaultValue,
                         StateBinding<Boolean> stateBinding,
                         Collection<BoolVar> vars,
                         ProblemRepository problemRepository,
                         IntVar.ProviderFactory intProviderFactory) {
        super(key, defaultValue, stateBinding, vars, 1, 1,
                problemRepository, intProviderFactory);
    }

    public interface ProviderFactory {
        Provider create(Collection<BoolVar> boolVars);
    }

    public static class Provider implements ConditionProvider<XOrCondition> {

        private final IntVar.ProviderFactory intVarProviderFactory;

        private final Collection<BoolVar> boolVars;
        private Boolean defaultValue = true;

        @Inject
        public Provider(@Assisted Collection<BoolVar> boolVars,
                        IntVar.ProviderFactory intVarProviderFactory) {
            this.boolVars = boolVars;
            this.intVarProviderFactory = intVarProviderFactory;
        }

        public XOrCondition.Provider withDefault(Boolean defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        @Override
        public XOrCondition provide(Key key, StateBinding<Boolean> stateBinding, ProblemRepository problemRepository) {
            return new XOrCondition(key, defaultValue, stateBinding,
                    this.boolVars, problemRepository, this.intVarProviderFactory);
        }

        @Override
        public Key getDefaultKey() {
            return CompositeKey.create()
                    .with(TypeProperty.of(XOrCondition.class))
                    .with(KeyProperty.of("vars", StringKey.joinKeyProviders(this.boolVars)));
        }
    }
}