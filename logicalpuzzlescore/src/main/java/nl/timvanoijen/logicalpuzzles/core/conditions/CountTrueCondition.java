package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.TypeProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;
import nl.timvanoijen.logicalpuzzles.core.variables.*;

import java.util.Collection;


public class CountTrueCondition extends BoolVar implements Condition {

    private final Collection<BoolVar> vars;
    private final int minTrue;
    private final int maxTrue;

    // Meta-data vars that count the number of true and false variables.
    private final IntVar nrDetectedTrue;
    private final IntVar nrDetectedFalse;

    public CountTrueCondition(Key key, Boolean defaultValue, StateBinding<Boolean> stateBinding,
                              Collection<BoolVar> vars, int minTrue, int maxTrue,
                              ProblemRepository problemRepository,
                              IntVar.ProviderFactory intProviderFactory) {
        super(key, defaultValue, stateBinding);

        ConditionUtils.verifyVariablesAreFinal(vars);
        this.vars = vars;
        this.minTrue = minTrue;
        this.maxTrue = maxTrue;
        this.nrDetectedTrue = problemRepository.createVariable(
                GuidKey.create(), intProviderFactory.create().withDefault(0));
        this.nrDetectedFalse = problemRepository.createVariable(
                GuidKey.create(), intProviderFactory.create().withDefault(0));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterable<Variable> getInvolvedVariables() {
        return (Iterable<Variable>)(Iterable)this.vars;
    }

    @Override
    public boolean processUpdates(Iterable<Variable> changedVariables) {

        Boolean conditionValue = this.getValue();

        for (Variable<?> var : changedVariables) {
            if (var == this)
                continue;
            Boolean value = (Boolean)var.getValue();
            if (value == true)
                this.nrDetectedTrue.setValue(this.nrDetectedTrue.getValue() + 1);
            else if (value == false)
                this.nrDetectedFalse.setValue(this.nrDetectedFalse.getValue() + 1);
        }

        if (conditionValue == null)
            return this.processForUndeterminedCondition();
        if (conditionValue == true)
            return this.processForTrueCondition();
        return this.processForFalseCondition();
    }

    private boolean processForTrueCondition() {
        if (this.nrDetectedTrue.getValue() == this.maxTrue) {
            this.makeUndeterminedFalse();
            return true;
        }

        if (this.nrDetectedFalse.getValue() == this.vars.size() - this.minTrue) {
            this.makeUndeterminedTrue();
            return true;
        }

        if (this.nrDetectedFalse.getValue() > this.vars.size() - this.minTrue)
            return false;

        return this.nrDetectedTrue.getValue() <= this.maxTrue;
    }

    private boolean processForFalseCondition() {
        if (this.nrDetectedTrue.getValue() >= this.minTrue) {
            int maxReachable = this.vars.size() - this.nrDetectedFalse.getValue();
            if (maxReachable == this.maxTrue + 1) {
                this.makeUndeterminedTrue();
                return true;
            }
            if (maxReachable <= this.maxTrue) {
                return false;
            }
        }

        if (this.nrDetectedFalse.getValue() >= this.vars.size() - this.maxTrue) {
            int minReachable = this.nrDetectedTrue.getValue();
            if (minReachable == this.minTrue - 1) {
                this.makeUndeterminedFalse();
                return true;
            }
            return minReachable < this.minTrue;
        }
        return true;
    }

    private boolean processForUndeterminedCondition() {
        // Only true possibility: min reach and max reach should be in min-max interval.
        if (this.nrDetectedTrue.getValue() >= this.minTrue &&
                this.vars.size() - this.nrDetectedFalse.getValue() <= this.maxTrue) {
            this.setValue(true);
        } else if (this.nrDetectedTrue.getValue() > this.maxTrue) {
            // If necessarily larger than max, set condition value to false
            this.setValue(false);
        } else if (this.nrDetectedFalse.getValue() > this.vars.size() - this.minTrue) {
            // If necessarily smaller than min, set condition value to false.
            this.setValue(false);
        }

        return true;
    }

    private void makeUndeterminedFalse() {
        for (BoolVar var : vars) {
            if (var.getValue() == null)
                var.setValue(false);
        }
    }

    private void makeUndeterminedTrue() {
        for (BoolVar var : vars) {
            if (var.getValue() == null)
                var.setValue(true);
        }
    }

    public interface ProviderFactory {
         Provider create(Collection<BoolVar> boolVars);
    }

    public static class Provider implements ConditionProvider<CountTrueCondition> {

        private final IntVar.ProviderFactory intVarProviderFactory;

        private final Collection<BoolVar> boolVars;
        private int minTrue = 1;
        private int maxTrue = 1;
        private Boolean defaultValue = true;

        @Inject
        public Provider(@Assisted Collection<BoolVar> boolVars,
                        IntVar.ProviderFactory intVarProviderFactory) {
            this.boolVars = boolVars;
            this.intVarProviderFactory = intVarProviderFactory;
        }

        public Provider withMinTrue(int minTrue) {
            this.minTrue = minTrue;
            return this;
        }

        public Provider withMaxTrue(int maxTrue) {
            this.maxTrue = maxTrue;
            return this;
        }

        public Provider withDefault(Boolean defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        @Override
        public CountTrueCondition provide(Key key, StateBinding<Boolean> stateBinding, ProblemRepository problemRepository) {
            return new CountTrueCondition(key, defaultValue, stateBinding,
                    this.boolVars, this.minTrue, this.maxTrue,
                    problemRepository, this.intVarProviderFactory);
        }

        @Override
        public Key getDefaultKey() {
            return CompositeKey.create()
                    .with(TypeProperty.of(CountTrueCondition.class))
                    .with(KeyProperty.of("vars", StringKey.joinKeyProviders(this.boolVars)))
                    .with(KeyProperty.of("min", IntKey.of(this.minTrue)))
                    .with(KeyProperty.of("max", IntKey.of(this.maxTrue)));
        }
    }
}