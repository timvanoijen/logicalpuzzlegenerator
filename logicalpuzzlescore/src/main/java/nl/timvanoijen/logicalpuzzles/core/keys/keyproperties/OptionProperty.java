package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import nl.timvanoijen.logicalpuzzles.core.keys.IntKey;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;

import javax.annotation.concurrent.Immutable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Immutable
public class OptionProperty extends KeyProperty {

    private final static String name = "option";

    public static OptionProperty of(int option) {
        return new OptionProperty(option);
    }

    public static Iterable<KeyProperty> range(int from, int till) {
        return IntStream.range(from, till + 1).mapToObj(OptionProperty::of)
                .collect(Collectors.toList());
    }

    public static Extractor extract() { return new Extractor(); }

    private OptionProperty(int option) {
        super(name, IntKey.of(option));
    }

    public static class Extractor implements KeyPropertyExtractor<Integer> {
        @Override
        public Integer extract(Key key) {
            return Integer.parseInt(key.get(name).getValue());
        }
    }
}
