package nl.timvanoijen.logicalpuzzles.core.solvers;

import nl.timvanoijen.logicalpuzzles.core.problems.Problem;

public interface SolvingStrategy {
    SolvingResult solve(Problem problem,
                        SolvingContext solvingContext);
}
