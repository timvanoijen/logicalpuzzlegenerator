package nl.timvanoijen.logicalpuzzles.core.keys.generators;

import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.ColumnProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.RowProperty;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GridKeysGenerator {

    private final Key rootKey;

    @Getter
    private final int width;
    @Getter
    private final int height;

    public GridKeysGenerator(Key rootKey, int width, int height) {
        this.rootKey = rootKey;
        this.width = width;
        this.height = height;
    }

    public Iterable<? extends Iterable<Key>> rowGroups() {
        return KeyBuilder
                .from(this.rootKey)
                .fork(ColumnProperty.range(1, this.width))
                .fork(RowProperty.range(1, this.height))
                .build();
    }

    public Iterable<Key> rowGroup(int row) {
        return KeyBuilder
                .from(this.rootKey)
                .fork(ColumnProperty.range(1, this.width))
                .with(RowProperty.of(row))
                .build();
    }

    public Iterable<? extends Iterable<Key>> columnGroups() {
        return KeyBuilder
                .from(this.rootKey)
                .fork(RowProperty.range(1, this.height))
                .fork(ColumnProperty.range(1, this.width))
                .build();
    }

    public Iterable<Key> columnGroup(int column) {
        return KeyBuilder
                .from(this.rootKey)
                .fork(RowProperty.range(1, this.height))
                .with(ColumnProperty.of(column))
                .build();
    }

    public Iterable<Key> elements() {
        return KeyBuilder
                .from(this.rootKey)
                .fork(RowProperty.range(1, this.height))
                .fork(ColumnProperty.range(1, this.width))
                .build().flatten();
    }

    public Key element(int column, int row) {
        return KeyBuilder
                .from(this.rootKey)
                .with(RowProperty.of(row))
                .with(ColumnProperty.of(column))
                .build();
    }

    public Iterable<Key> diagonalTopLeftGroup() {
        if (this.width != this.height)
            throw new DiagonalSelectException();

        return IntStream.range(1, this.width + 1)
                .mapToObj(i -> this.element(i,i))
                .collect(Collectors.toList());
    }

    public Iterable<Key> diagonalBottomLeftGroup() {
        if (this.width != this.height)
            throw new DiagonalSelectException();

        return IntStream.range(1, this.width + 1)
                .mapToObj(i -> this.element(i, this.width + 1 - i))
                .collect(Collectors.toList());
    }

    public class DiagonalSelectException extends RuntimeException {
        public DiagonalSelectException() {
            super("Diagonal cannot be selected when width and height are unequal");
        }
    }
}
