package nl.timvanoijen.logicalpuzzles.core.state;

import lombok.Getter;

public class LocalExecutionScope implements AutoCloseable{

    @Getter
    private final Repository repository;

    private static final ThreadLocal<LocalExecutionScope> current = new ThreadLocal<>();

    LocalExecutionScope(Repository repository) {
        this.repository = repository;
        if (LocalExecutionScope.current.get() != null)
            throw new RuntimeException("Calculation scope cannot be nested");
        LocalExecutionScope.current.set(this);
    }

    static LocalExecutionScope getCurrent() {
        return LocalExecutionScope.current.get();
    }

    @Override
    public void close() {
        LocalExecutionScope.current.remove();
    }
}
