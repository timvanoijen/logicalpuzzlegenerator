package nl.timvanoijen.logicalpuzzles.core.problems;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

@Getter
@AllArgsConstructor
public class Problem{
    private Iterable<Variable> variables;
    private Iterable<Condition> conditions;
}
