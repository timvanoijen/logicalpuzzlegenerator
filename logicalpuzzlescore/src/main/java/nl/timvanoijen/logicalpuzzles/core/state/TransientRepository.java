package nl.timvanoijen.logicalpuzzles.core.state;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

class TransientRepository implements Repository {

    private final Repository sink;

    private final Map<Integer, Object> updates = new HashMap<>();

    public TransientRepository(Repository sink) {
        this.sink = sink;
    }

    @Override
    public void setStateVarValue(int id, Object val) {
        this.updates.put(id, val);
    }

    @Override
    public Object getStateVarValue(int id) {
        if(this.updates.containsKey(id))
            return this.updates.get(id);
        return this.sink.getStateVarValue(id);
    }

    @Override
    public long getStateVarLastModified(int id) {
        throw new RuntimeException("Not supported");
    }

    @Override
    public boolean isFinal(int id) {
        return this.sink.isFinal(id);
    }

    @Override
    public int createStateVar(boolean isFinal) {
        throw new RuntimeException("Not supported");
    }

    @Override
    public int newVersion() {
        throw new RuntimeException("Not supported");
    }

    @Override
    public void rollbackToVersion(int version) {
        throw new RuntimeException("Not supported");
    }

    @Override
    public void observe(int id, BiConsumer<Integer, Object> consumer) {
        throw new RuntimeException("Not supported");
    }

    @Override
    public void unobserve(int id, BiConsumer<Integer, Object> consumer) {
        throw new RuntimeException("Not supported");
    }

    void flush() {
        for(Map.Entry<Integer, Object> e : this.updates.entrySet())
            this.sink.setStateVarValue(e.getKey(), e.getValue());
        this.updates.clear();
    }
}
