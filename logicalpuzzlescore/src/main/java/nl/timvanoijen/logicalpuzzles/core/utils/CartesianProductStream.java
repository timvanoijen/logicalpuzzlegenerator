package nl.timvanoijen.logicalpuzzles.core.utils;

import com.google.common.collect.Streams;
import org.javatuples.Pair;
import org.javatuples.Triplet;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CartesianProductStream {
    public static <T1,T2> Stream<Pair<T1,T2>> of(Iterable<T1> t1, Iterable<T2> t2) {
        return s(t1).flatMap(v1 -> s(t2).map(v2 -> new Pair<>(v1, v2)));
    }

    public static <T1,T2> Stream<Pair<T1,T2>> of(Stream<T1> t1, Stream<T2> t2) {
        return of(i(t1), i(t2));
    }

    public static <T1, T2, T3> Stream<Triplet<T1,T2,T3>> of(Iterable<T1> t1, Iterable<T2> t2, Iterable<T3> t3) {
        return of(of(t1,t2)::iterator,t3).map(p ->
                new Triplet<>(p.getValue0().getValue0(),
                              p.getValue0().getValue1(),
                              p.getValue1()));
    }

    public static <T1, T2, T3> Stream<Triplet<T1,T2,T3>> of(Stream<T1> t1, Stream<T2> t2, Stream<T3> t3) {
        return of(i(t1), i(t2), i(t3));
    }

    private static <T> Stream<T> s(Iterable<T> i) {
        return Streams.stream(i);
    }

    private static <T> Iterable<T> i(Stream<T> s) {
        return s.collect(Collectors.toList());
    }
}
