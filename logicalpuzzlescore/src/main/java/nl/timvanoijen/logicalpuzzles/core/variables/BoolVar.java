package nl.timvanoijen.logicalpuzzles.core.variables;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;


public class BoolVar extends StateBoundVariable<Boolean> {

    protected BoolVar(Key key, Boolean defaultValue,
                    StateBinding<Boolean> stateBinding) {
        super(key, defaultValue, stateBinding);
    }

    public interface ProviderFactory {
        BoolVar.Provider create();
    }

    public static class Provider implements VariableProvider<Boolean, BoolVar> {

        private Boolean defaultValue = null;

        @Inject
        private Provider() {}

        public Provider withDefault(Boolean defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        @Override
        public BoolVar provide(Key key, StateBinding<Boolean> stateBinding) {
            return new BoolVar(key, this.defaultValue, stateBinding);
        }
    }
}

