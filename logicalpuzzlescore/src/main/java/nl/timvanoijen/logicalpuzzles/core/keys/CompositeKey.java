package nl.timvanoijen.logicalpuzzles.core.keys;

import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.KeyProperty;

import javax.annotation.concurrent.Immutable;
import java.util.*;

@Immutable
public class CompositeKey implements Key {

    private final Map<String, KeyProperty> properties = new HashMap<>();
    private final String value;
    private String cachedString = null;

    protected CompositeKey(Key key) {
        key.getProperties().forEach(this::addProperty);
        this.value = key.getValue();
    }

    public static CompositeKey create() {
        return new CompositeKey(EmptyKey.create());
    }

    public static CompositeKey from(Key key) {
        return new CompositeKey(key);
    }

    public CompositeKey with(KeyProperty property) {
        var result = new CompositeKey(this);
        result.addProperty(property);
        return result;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public <T extends Key> T get(String propertyName) {
        var result = this.properties.computeIfAbsent(propertyName,
            p -> {
                throw new KeyException(String.format("Key has no property %s", propertyName));
            }).getKey();
        try {
            //noinspection unchecked
            return (T)result;
        } catch(ClassCastException e) {
            throw new KeyException(String.format("Key property %s has wrong type", propertyName));
        }
    }

    @Override
    public Collection<KeyProperty> getProperties() {
        return this.properties.values();
    }

    // We cache the asString() evaluation result.
    @Override
    public String asString() {
        if (this.cachedString == null)
            this.cachedString = Key.super.asString();
        return this.cachedString;
    }

    protected void addProperty(KeyProperty property) {
        // Adding a property is only allowed during building
        // process when the asString() method has not been called
        // yet (only relevant for inheriting classes).
        assert this.cachedString == null;
        this.properties.put(property.getName(), property);
    }
}
