package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;


@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class KeyProperty {
    private final String name;
    private final Key key;

    public static KeyProperty of(String name, Key key) {
        return new KeyProperty(name, key);
    }

    public static KeyProperty of(Key key) { return of("key", key); }
}
