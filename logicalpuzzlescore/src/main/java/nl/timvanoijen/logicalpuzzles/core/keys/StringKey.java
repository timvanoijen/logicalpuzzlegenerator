package nl.timvanoijen.logicalpuzzles.core.keys;

import com.google.common.collect.Streams;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import javax.annotation.concurrent.Immutable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Immutable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class StringKey implements Key {

    private final String value;

    @Override
    public String getValue() {
        return this.value;
    }

    public static StringKey of(String value) {
        return new StringKey(value);
    }

    public static Iterable<Key> range(String format, int from, int till) {
        return IntStream.range(from, till + 1)
                .mapToObj(i -> StringKey.of(String.format(format, i)))
                .collect(Collectors.toList());
    }

    public static StringKey joinStrings(Iterable<String> values) {
        return new StringKey(String.join(";", values));
    }

    public static StringKey joinKeys(Iterable<Key> values) {
        return joinStrings(Streams.stream(values)
                .map(Key::asString).collect(Collectors.toList()));
    }

    public static StringKey joinKeyProviders(Iterable<? extends KeyProvider> values) {
        return joinStrings(Streams.stream(values)
                .map(k -> k.getKey().asString()).collect(Collectors.toList()));
    }
}
