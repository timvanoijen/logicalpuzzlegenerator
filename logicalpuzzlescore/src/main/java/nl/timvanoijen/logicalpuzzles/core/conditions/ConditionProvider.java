package nl.timvanoijen.logicalpuzzles.core.conditions;

import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateBinding;

public interface ConditionProvider<T extends Condition> {
    T provide(Key key,
              StateBinding<Boolean> stateBinding,
              ProblemRepository problemRepository);

    Key getDefaultKey();
}
