package nl.timvanoijen.logicalpuzzles.core.state;

import java.util.function.Consumer;

public interface StateBinding<T> {

    int getId();
    T getValue();
    void setValue(T value);
    boolean isFinal();
    long getLastModified();
    void observe(Consumer<T> observer);
    void unobserve(Consumer<T> observer);
}
