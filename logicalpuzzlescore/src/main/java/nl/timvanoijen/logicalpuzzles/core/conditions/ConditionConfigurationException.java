package nl.timvanoijen.logicalpuzzles.core.conditions;

public class ConditionConfigurationException extends RuntimeException {
    public ConditionConfigurationException(String reason) {
        super(String.format("Condition wrongly configured: %s", reason));
    }
}
