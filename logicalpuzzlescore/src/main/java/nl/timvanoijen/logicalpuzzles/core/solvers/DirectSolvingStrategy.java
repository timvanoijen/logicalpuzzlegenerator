package nl.timvanoijen.logicalpuzzles.core.solvers;

import com.google.common.collect.Streams;
import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.state.LocalExecutionScope;
import nl.timvanoijen.logicalpuzzles.core.state.StateReassignedException;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.*;

public class DirectSolvingStrategy implements SolvingStrategy {

    @Inject
    public DirectSolvingStrategy() { }

    @Override
    public SolvingResult solve(Problem problem, SolvingContext solvingContext) {
        return (new DirectSolvingStrategyInstance(problem, solvingContext)).solve();
    }

    private class DirectSolvingStrategyInstance {
        private final Problem problem; // TODO: allow for sub-problem solving
        private final SolvingContext solvingContext;

        public DirectSolvingStrategyInstance(Problem problem, SolvingContext solvingContext) {
            this.problem = problem;
            this.solvingContext = solvingContext;
        }

        public SolvingResult solve() {
            try {
                boolean proceed = true;
                while (proceed) {
                    try (var context = solvingContext.provideExecutionContext()) {
                        proceed = false;
                        Map.Entry<Condition, List<Variable>> entry;
                        while ((entry = this.solvingContext.pollConditionVariableChanges()) != null) {
                            proceed = true;
                            Condition condition = entry.getKey();
                            List<Variable> changedVariables = entry.getValue();

                            // Process condition in local scope, such that updates that
                            // the condition performs are only flushed after all conditions
                            // have been evaluated.
                            try (LocalExecutionScope scope = context.createLocalScope()) {
                                if (!condition.processUpdates(changedVariables)) {
                                    return SolvingResult.failed();
                                }
                            }
                        }
                    } catch (StateReassignedException e) {
                        // Two different conditions concluded a different value
                        // for the same variable.
                        return SolvingResult.failed();
                    }
                }
                if (Streams.stream(this.problem.getVariables()).allMatch(v -> v.getValue() != null))
                    return SolvingResult.succeeded();
                return SolvingResult.undetermined();
            } finally {
                // Make sure that in failing scenario, all condition variable changes
                // have been 'processed'.
                while(this.solvingContext.pollConditionVariableChanges() != null) { }
            }
        }
    }

    public interface Factory {
        DirectSolvingStrategy create();
    }
}
