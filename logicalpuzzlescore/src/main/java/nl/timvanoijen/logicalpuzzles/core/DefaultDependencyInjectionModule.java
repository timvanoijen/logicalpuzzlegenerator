package nl.timvanoijen.logicalpuzzles.core;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import nl.timvanoijen.logicalpuzzles.core.conditions.*;
import nl.timvanoijen.logicalpuzzles.core.problems.providers.IntegerOptionsProvider;
import nl.timvanoijen.logicalpuzzles.core.problems.providers.UniqueValuesConditionProvider;
import nl.timvanoijen.logicalpuzzles.core.solvers.DirectSolvingStrategy;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingContext;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;


public class DefaultDependencyInjectionModule extends AbstractModule {

    @Override
    protected void configure() {

        // ------------------------------------------
        // -- Build factories
        // ------------------------------------------

        // Variables
        install(new FactoryModuleBuilder().build(IntVar.ProviderFactory.class));
        install(new FactoryModuleBuilder().build(BoolVar.ProviderFactory.class));

        // Conditions
        install(new FactoryModuleBuilder().build(AndCondition.ProviderFactory.class));
        install(new FactoryModuleBuilder().build(CountTrueCondition.ProviderFactory.class));
        install(new FactoryModuleBuilder().build(FalseCondition.ProviderFactory.class));
        install(new FactoryModuleBuilder().build(IntegerOptionCondition.ProviderFactory.class));
        install(new FactoryModuleBuilder().build(OrCondition.ProviderFactory.class));
        install(new FactoryModuleBuilder().build(XOrCondition.ProviderFactory.class));

        // Problem providers
        install(new FactoryModuleBuilder().build(IntegerOptionsProvider.Factory.class));
        install(new FactoryModuleBuilder().build(UniqueValuesConditionProvider.Factory.class));

        // Other
        install(new FactoryModuleBuilder().build(SolvingContext.Factory.class));
        install(new FactoryModuleBuilder().build(DirectSolvingStrategy.Factory.class));
    }
}
