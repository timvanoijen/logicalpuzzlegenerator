package nl.timvanoijen.logicalpuzzles.core.conditions;

import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.Objects;
import java.util.stream.StreamSupport;

class ConditionUtils {

    public static void verifyConditionValueIsTrue(Condition condition) {
        if (!Objects.equals(condition.getValue(), true))
            throw new ConditionConfigurationException(
                    String.format("%s not allowed to be non-true",
                            condition.getClass().toString()));
    }

    public static void verifyVariableIsFinal(Variable variable) {
        if (!variable.isFinal())
            throw new ConditionConfigurationException("Variable should be final.");
    }

    public static void verifyVariablesAreFinal(Iterable<? extends Variable> variables) {
        StreamSupport.stream(variables.spliterator(), false)
                .forEach(ConditionUtils::verifyVariableIsFinal);
    }
}
