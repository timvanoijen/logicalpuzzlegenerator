package nl.timvanoijen.logicalpuzzles.core.state;


import java.util.function.BiConsumer;

public class ProxyRepository implements Repository {

    private final SinkRepository sinkRepository;

    public ProxyRepository(SinkRepository sinkRepository) {
        this.sinkRepository = sinkRepository;
    }

    @Override
    public void setStateVarValue(int id, Object val) {
        this.getRepository().setStateVarValue(id, val);
    }

    @Override
    public Object getStateVarValue(int id) {
        return this.getRepository().getStateVarValue(id);
    }

    @Override
    public long getStateVarLastModified(int id) { return this.getRepository().getStateVarLastModified(id); }

    @Override
    public boolean isFinal(int id) { return this.getRepository().isFinal(id); }

    @Override
    public int createStateVar(boolean isFinal) {
        return this.getRepository().createStateVar(isFinal);
    }

    @Override
    public int newVersion() {
        return this.getRepository().newVersion();
    }

    @Override
    public void rollbackToVersion(int version) {
        this.getRepository().rollbackToVersion(version);
    }

    @Override
    public void observe(int id, BiConsumer<Integer, Object> consumer) {
        this.getRepository().observe(id, consumer);
    }

    @Override
    public void unobserve(int id, BiConsumer<Integer, Object> consumer) {
        this.getRepository().unobserve(id, consumer);
    }

    private Repository getRepository() {
        LocalExecutionScope cs = LocalExecutionScope.getCurrent();
        return cs != null ? cs.getRepository() : this.sinkRepository;
    }
}
