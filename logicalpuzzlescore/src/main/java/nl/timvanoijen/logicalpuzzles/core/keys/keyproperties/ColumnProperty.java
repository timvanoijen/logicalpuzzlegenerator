package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import nl.timvanoijen.logicalpuzzles.core.keys.IntKey;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;

import javax.annotation.concurrent.Immutable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Immutable
public class ColumnProperty extends KeyProperty {

    private final static String name = "col";

    public static ColumnProperty of(int col) {
        return new ColumnProperty(col);
    }

    public static Iterable<KeyProperty> range(int from, int till) {
        return IntStream.range(from, till + 1).mapToObj(ColumnProperty::of)
                .collect(Collectors.toList());
    }

    public static Extractor extract() { return new Extractor(); }

    private ColumnProperty(int col) {
        super(name, IntKey.of(col));
    }

    public static class Extractor implements KeyPropertyExtractor<Integer> {
        @Override
        public Integer extract(Key key) {
            return Integer.parseInt(key.get(name).getValue());
        }
    }
}
