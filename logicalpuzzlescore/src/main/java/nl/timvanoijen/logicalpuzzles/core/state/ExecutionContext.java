package nl.timvanoijen.logicalpuzzles.core.state;

import java.util.ArrayList;
import java.util.List;

public class ExecutionContext implements AutoCloseable {

    private final SinkRepository sinkRepository;
    private final List<TransientRepository> transientRepositories = new ArrayList<>();

    public ExecutionContext(SinkRepository sinkRepository) {
        this.sinkRepository = sinkRepository;
    }

    public int newVersion() {
        return this.sinkRepository.newVersion();
    }

    public void rollbackToVersion(int version) {
        this.sinkRepository.rollbackToVersion(version);
    }

    public LocalExecutionScope createLocalScope() {
        TransientRepository transientRepository = new TransientRepository(this.sinkRepository);
        this.transientRepositories.add(transientRepository);
        return new LocalExecutionScope(transientRepository);
    }

    @Override
    public void close() {
        for(TransientRepository t : this.transientRepositories)
            t.flush();
    }
}
