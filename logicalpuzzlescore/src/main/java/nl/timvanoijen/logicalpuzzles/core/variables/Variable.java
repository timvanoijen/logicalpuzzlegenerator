package nl.timvanoijen.logicalpuzzles.core.variables;

import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.keys.KeyProvider;

import java.util.function.Consumer;


public interface Variable<T> extends KeyProvider {
    Key getKey();

    int getId();
    T getValue();
    void setValue(T value);
    boolean isFinal();
    long getLastModified();

    void observe(Consumer<T> observer);
    void unobserve(Consumer<T> observer);
}
