package nl.timvanoijen.logicalpuzzles.core.keys.keyproperties;

import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;

import javax.annotation.concurrent.Immutable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Immutable
public class StringProperty extends KeyProperty {

    public static StringProperty of(String name, String value) {
        return new StringProperty(name, value);
    }

    public static Iterable<StringProperty> range(String nameFormat, String valueFormat,
                                                 int from, int till) {
        return IntStream.range(from, till + 1)
                .mapToObj(i -> StringProperty.of(
                        String.format(nameFormat, i),
                        String.format(valueFormat, i)))
                .collect(Collectors.toList());
    }

    private StringProperty(String name, String value) {
        super(name, StringKey.of(value));
    }
}

