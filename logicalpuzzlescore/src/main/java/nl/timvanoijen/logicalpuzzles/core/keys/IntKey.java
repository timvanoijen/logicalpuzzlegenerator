package nl.timvanoijen.logicalpuzzles.core.keys;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import javax.annotation.concurrent.Immutable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Immutable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class IntKey implements Key {

    private final String value;

    @Override
    public String getValue() {
        return this.value;
    }

    public static IntKey of(int intValue) {
        return new IntKey(Integer.toString(intValue));
    }

    public static Iterable<Key> range(int from, int till) {
        return IntStream.range(from, till + 1)
                .mapToObj(i -> IntKey.of(i))
                .collect(Collectors.toList());
    }
}
