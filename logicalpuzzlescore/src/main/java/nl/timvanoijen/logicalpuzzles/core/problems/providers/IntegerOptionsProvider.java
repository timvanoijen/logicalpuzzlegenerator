package nl.timvanoijen.logicalpuzzles.core.problems.providers;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.conditions.IntegerOptionCondition;
import nl.timvanoijen.logicalpuzzles.core.conditions.XOrCondition;
import nl.timvanoijen.logicalpuzzles.core.keys.CompositeKey;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.OptionProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemCollector;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemProvider;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;

import java.util.ArrayList;
import java.util.List;

public class IntegerOptionsProvider implements ProblemProvider {

    private final Key integerKey;
    private final BoolVar.ProviderFactory boolProviderFactory;
    private final XOrCondition.ProviderFactory xOrConditionProviderFactory;
    private final IntegerOptionCondition.ProviderFactory integerOptionConditionProviderFactory;

    @Inject
    private IntegerOptionsProvider(@Assisted Key integerKey,
                                  BoolVar.ProviderFactory boolProviderFactory,
                                  XOrCondition.ProviderFactory xOrConditionProviderFactory,
                                  IntegerOptionCondition.ProviderFactory integerOptionConditionProviderFactory) {
        this.integerKey = integerKey;
        this.boolProviderFactory = boolProviderFactory;
        this.xOrConditionProviderFactory = xOrConditionProviderFactory;
        this.integerOptionConditionProviderFactory = integerOptionConditionProviderFactory;
    }

    @Override
    public Problem provide(ProblemRepository problemRepository) {

        // Retrieve integer and its range
        IntVar intVar = problemRepository.getVariable(this.integerKey);
        IntVar.Range range = intVar.getRange();
        assert range != null;

        List<BoolVar> optionVars = new ArrayList<>();
        List<Condition> optionConditions = new ArrayList<>();
        for(int i = range.getFrom(); i <= range.getTill(); i++) {
            Key optionKey = CompositeKey.from(this.integerKey)
                    .with(OptionProperty.of(i));

            // Ensure boolean options
            BoolVar optionVar = problemRepository.getOrCreateFinalVariable(optionKey,
                    this.boolProviderFactory.create());
            optionVars.add(optionVar);

            // Ensure equivalence conditions
            Condition integerOptionCondition = problemRepository.getOrCreateCondition(
                    this.integerOptionConditionProviderFactory
                            .create(intVar, optionVar, i));
            optionConditions.add(integerOptionCondition);
        }

        // Ensure xor-condition of options (exactly 1 should be true)
        var xorCondition = problemRepository.getOrCreateCondition(this.xOrConditionProviderFactory
                .create(optionVars));

        // Collect variables and conditions as the sub-problem to be returned
        return new ProblemCollector()
                .addVariable(intVar)
                .addVariables(optionVars)
                .addConditions(optionConditions)
                .addCondition(xorCondition)
                .getProblem();
    }

    public interface Factory {
        IntegerOptionsProvider create(Key integerKey);
    }
}
