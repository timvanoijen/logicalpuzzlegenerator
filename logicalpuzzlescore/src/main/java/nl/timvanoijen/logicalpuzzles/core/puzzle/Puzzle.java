package nl.timvanoijen.logicalpuzzles.core.puzzle;

import nl.timvanoijen.logicalpuzzles.core.problems.Problem;

public interface Puzzle {
    Problem getProblem();
}
