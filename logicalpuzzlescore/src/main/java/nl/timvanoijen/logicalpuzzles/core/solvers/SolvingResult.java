package nl.timvanoijen.logicalpuzzles.core.solvers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class SolvingResult {
    private boolean succeeded;
    private boolean failed;

    public static SolvingResult succeeded() {
        return new SolvingResult(true, false);
    }

    public static SolvingResult failed() {
        return new SolvingResult(false, true);
    }

    public static SolvingResult undetermined() {
        return new SolvingResult(false, false);
    }
}
