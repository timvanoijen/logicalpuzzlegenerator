package nl.timvanoijen.logicalpuzzles.core;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.jupiter.api.BeforeEach;


public class TestBase {

    @BeforeEach
    public void init () {
        Injector injector = Guice.createInjector(new TestDependencyInjectionModule());
        injector.injectMembers(this);
    }
}
