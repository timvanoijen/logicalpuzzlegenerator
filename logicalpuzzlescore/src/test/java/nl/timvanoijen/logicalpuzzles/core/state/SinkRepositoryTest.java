package nl.timvanoijen.logicalpuzzles.core.state;

import nl.timvanoijen.logicalpuzzles.core.TestBase;
import org.junit.jupiter.api.Test;

public class SinkRepositoryTest extends TestBase {

    @Test
    public void testVersioning() {

        SinkRepository repository = new SinkRepository();

        var var1 = repository.<Boolean>getBinding(repository.createStateVar(false));
        var var2 = repository.<Boolean>getBinding(repository.createStateVar(false));

        long tsvar1_version0 = var1.getLastModified();
        long tsvar2_version0 = var2.getLastModified();

        repository.newVersion();
        var1.setValue(true);
        var2.setValue(false);

        repository.newVersion();
        var1.setValue(false);

        repository.newVersion();
        var1.setValue(false);

        assert var1.getValue() == false;
        assert var2.getValue() == false;

        // Both vars have changed, but var1 was changed later.
        assert var1.getLastModified() > tsvar1_version0;
        assert var2.getLastModified() > tsvar2_version0;
        assert var1.getLastModified() > var2.getLastModified();

        repository.rollbackToVersion(1);

        assert var1.getValue() == true;
        assert var2.getValue() == false;

        // Both vars have changed, but var2 was changed later.
        assert var1.getLastModified() > tsvar1_version0;
        assert var2.getLastModified() > tsvar2_version0;
        assert var2.getLastModified() > var1.getLastModified();

        repository.rollbackToVersion(0);

        assert var1.getValue() == null;
        assert var2.getValue() == null;

        assert var1.getLastModified() == tsvar1_version0;
        assert var2.getLastModified() == tsvar2_version0;
    }

    @Test
    public void testRollbackAfterMultipleChangesOnSameVar() {
        SinkRepository repository = new SinkRepository();

        var var1 = repository.<Integer>getBinding(repository.createStateVar(false));

        // Set initial value
        var1.setValue(10);

        // New version
        repository.newVersion();
        assert var1.getValue() == 10;

        // Update twice
        var1.setValue(11);
        var1.setValue(12);
        assert var1.getValue() == 12;

        // Rollback to initial version
        repository.rollbackToVersion(0);

        assert var1.getValue() == 10;
    }
}
