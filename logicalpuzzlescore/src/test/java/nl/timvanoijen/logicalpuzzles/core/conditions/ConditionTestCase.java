package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import lombok.Setter;
import lombok.experimental.Accessors;
import nl.timvanoijen.logicalpuzzles.core.keys.KeyBuilder;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.state.StateReassignedException;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Accessors(chain=true)
public class ConditionTestCase {

    interface ConditionBuilder {
        Condition build(Boolean value, List<Variable> vars);
    }

    private final BoolVar.ProviderFactory boolVarProviderFactory;
    private final Random random;
    private final ProblemRepository problemRepository;
    private final List<Map.Entry<Integer, Object>> varAsserts = new ArrayList<>();

    @Setter private Variable[] vars;
    @Setter private Object[] updates;
    @Setter private ConditionBuilder conditionBuilder;
    @Setter private Boolean conditionValue = true;
    @Setter private boolean resultAssert = true;
    @Setter private Boolean conditionValueAssert;
    @Setter private boolean conditionValueIsNullAssert;

    public ConditionTestCase(BoolVar.ProviderFactory boolVarProviderFactory,
                             Random random,
                             ProblemRepository problemRepository) {
        this.boolVarProviderFactory = boolVarProviderFactory;
        this.random = random;
        this.problemRepository = problemRepository;
    }

    public ConditionTestCase withVarAssert(Integer idx, Object expected) {
        this.varAsserts.add(new AbstractMap.SimpleEntry<>(idx, expected));
        return this;
    }

    public void execute() {
        List<Variable> vars = this.vars != null ?
                Arrays.asList(this.vars) : this.createBoolVars(this.updates.length);

        Condition condition = this.conditionBuilder.build(
                this.conditionValue, vars);
        boolean result = this.execute(vars, updates, condition, this.random);

        assert result == this.resultAssert;
        assert this.conditionValueAssert == null || condition.getValue() == this.conditionValueAssert;
        assert !this.conditionValueIsNullAssert || condition.getValue() == null;

        for (Map.Entry<Integer, Object> varAssert : this.varAsserts)
            assert vars.get(varAssert.getKey()).getValue() == varAssert.getValue();
    }

    private List<Variable> createBoolVars(int nr) {
        List<Variable> result = new ArrayList<>(this.problemRepository.createFinalVariables(
                KeyBuilder.forkFrom(StringKey.range("b%d", 1, nr)).build(),
                this.boolVarProviderFactory.create()));
        return result;
    }

    public boolean execute(List<Variable> vars, Object[] updates, Condition condition, Random random) {

        List<List<Map.Entry<Integer, Object>>> updateBatches = this.generateBatches(updates, random);

        try {
            for (List<Map.Entry<Integer, Object>> batch : updateBatches) {
                List<Variable> updatedVars = new ArrayList<>();
                for (Map.Entry<Integer, Object> entry : batch) {
                    if (entry.getValue() == null)
                        continue;

                    Variable updatedVar = vars.get(entry.getKey());
                    updatedVars.add(updatedVar);
                    updatedVar.setValue(entry.getValue());
                }
                if (!condition.processUpdates(updatedVars))
                    return false;
            }
            return true;
        } catch(StateReassignedException e) {
            return false;
        }
    }

    private List<List<Map.Entry<Integer, Object>>> generateBatches(Object[] values, Random random) {

        // Step 1 of randomization: randomize order.
        List<Integer> indexes = IntStream.range(0, values.length).boxed().collect(Collectors.toList());
        Collections.shuffle(indexes);
        List<Map.Entry<Integer, Object>> updates = new ArrayList<>();
        for(Integer idx : indexes)
            updates.add(new AbstractMap.SimpleEntry(idx, values[idx]));

        // Step 2 of randomization: break-up in batches.
        List<List<Map.Entry<Integer, Object>>> updateBatches = new ArrayList<>();
        for(Map.Entry<Integer, Object> entry : updates) {
            if (updateBatches.size() == 0 || random.nextBoolean())
                updateBatches.add(new ArrayList<>());
            updateBatches.get(updateBatches.size() - 1).add(entry);
        }

        return updateBatches;
    }

    public static class Factory {
        @Inject private BoolVar.ProviderFactory boolVarFactory;

        public ConditionTestCase create(Random random, ProblemRepository problemRepository) {
            return new ConditionTestCase(boolVarFactory, random, problemRepository);
        }
    }
}
