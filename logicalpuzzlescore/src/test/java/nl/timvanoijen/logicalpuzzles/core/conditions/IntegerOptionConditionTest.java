package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import lombok.Setter;
import lombok.experimental.Accessors;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;
import org.junit.jupiter.api.RepeatedTest;

import java.util.*;

public class IntegerOptionConditionTest extends TestBase {

    @Inject private IntegerOptionCondition.ProviderFactory integerBoolEquivalenceConditionProviderFactory;
    @Inject private IntVar.ProviderFactory intProviderFactory;
    @Inject private BoolVar.ProviderFactory boolProviderFactory;
    @Inject private ConditionTestCase.Factory testCaseFactory;

    private final ProblemRepository problemRepository = new ProblemRepository();
    private static final Random random = new Random(IntegerOptionConditionTest.class.getSimpleName().hashCode());

    @RepeatedTest(5)
    public void testBoolBecomesTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setVars(this.createVars())
                .setUpdates(new Object[] { 5, null })
                .setConditionBuilder(new ConditionBuilder().setEquivalenceValue(5))
                .withVarAssert(1, true)
                .execute();
    }

    @RepeatedTest(5)
    public void testBoolBecomesFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setVars(this.createVars())
                .setUpdates(new Object[] { 5, null })
                .setConditionBuilder(new ConditionBuilder().setEquivalenceValue(55))
                .withVarAssert(1, false)
                .execute();
    }

    @RepeatedTest(5)
    public void testIntBecomesValue() {
        testCaseFactory.create(random, this.problemRepository)
                .setVars(this.createVars())
                .setUpdates(new Object[] { null, true })
                .setConditionBuilder(new ConditionBuilder().setEquivalenceValue(101))
                .withVarAssert(0, 101)
                .execute();
    }

    @RepeatedTest(5)
    public void testError() {
        testCaseFactory.create(random, this.problemRepository)
                .setVars(this.createVars())
                .setUpdates(new Object[] { 5, true })
                .setConditionBuilder(new ConditionBuilder().setEquivalenceValue(101))
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testNoError() {
        testCaseFactory.create(random, this.problemRepository)
                .setVars(this.createVars())
                .setUpdates(new Object[] { 5, false })
                .setConditionBuilder(new ConditionBuilder().setEquivalenceValue(101))
                .setResultAssert(true)
                .execute();
    }

    private Variable[] createVars() {
        return new Variable[] {
            this.problemRepository.createFinalVariable(StringKey.of("i1"),
                this.intProviderFactory.create()),
            this.problemRepository.createFinalVariable(StringKey.of("ib"),
                this.boolProviderFactory.create())
        };
    }

    @Accessors(chain=true)
    private class ConditionBuilder implements ConditionTestCase.ConditionBuilder {
        @Setter private int equivalenceValue;

        public Condition build(Boolean value, List<Variable> vars) {
            return problemRepository.createCondition(
                    integerBoolEquivalenceConditionProviderFactory
                            .create((IntVar)vars.get(0), (BoolVar)vars.get(1), equivalenceValue));
        }
    }
}
