package nl.timvanoijen.logicalpuzzles.core.problems.providers;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.OptionProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.Key;
import nl.timvanoijen.logicalpuzzles.core.keys.KeyBuilder;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.solvers.DirectSolvingStrategy;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingContext;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingResult;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import org.junit.jupiter.api.Test;

public class IntegerOptionsProviderTest extends TestBase {

    @Inject private IntVar.ProviderFactory intProviderFactory;
    @Inject private DirectSolvingStrategy.Factory directSolvingStrategyFactory;
    @Inject private IntegerOptionsProvider.Factory integerOptionsProviderFactory;
    @Inject private SolvingContext.Factory solvingContextFactory;

    @Test
    void testIntegerOptionsGenerator() {
        var problemRepository = new ProblemRepository();

        Key key = StringKey.of("i1");

        problemRepository.createFinalVariable(key, this.intProviderFactory
                .create().withRange(new IntVar.Range(1, 4)));
        problemRepository.extend(this.integerOptionsProviderFactory.create(key));

        problemRepository.getVariable(KeyBuilder.from(key).with(OptionProperty.of(1)).build()).setValue(false);
        problemRepository.getVariable(KeyBuilder.from(key).with(OptionProperty.of(2)).build()).setValue(false);
        problemRepository.getVariable(KeyBuilder.from(key).with(OptionProperty.of(4)).build()).setValue(false);

        try(SolvingContext solvingContext = this.solvingContextFactory.create(problemRepository)) {

            var solvingStrategy = this.directSolvingStrategyFactory.create();
            SolvingResult result = solvingContext.solve(
                    problemRepository.getGrandProblem(), solvingStrategy);
            assert result.isSucceeded() == true;
            assert problemRepository.<BoolVar>getVariable(KeyBuilder.from(key)
                    .with(OptionProperty.of(3)).build())
                    .getValue() == true;
        }
    }
}
