package nl.timvanoijen.logicalpuzzles.core.state;

import nl.timvanoijen.logicalpuzzles.core.TestBase;
import org.junit.jupiter.api.Test;

public class ExecutionContextTest extends TestBase {

    @Test
    public void testLocalExecutionScope() {

        SinkRepository sinkRepository = new SinkRepository();
        ProxyRepository repository = new ProxyRepository(sinkRepository);

        var var1 = repository.<Boolean>getBinding(repository.createStateVar(false));
        var var2 = repository.<Boolean>getBinding(repository.createStateVar(false));

        repository.newVersion();

        try(ExecutionContext context = new ExecutionContext(sinkRepository)) {

            // In scope with transient repo nr 1, make var1 TRUE.
            try(LocalExecutionScope scope = context.createLocalScope()) {
                var1.setValue(true);
                assert var1.getValue() == true;
                assert var2.getValue() == null;
            }

            // Since the transient repo has not been flushed yet, outside the scope
            // the variables should be unchanged.
            assert var1.getValue() == null;
            assert var2.getValue() == null;

            // In scope with repo nr 2, make var2 FALSE.
            try (LocalExecutionScope scope = context.createLocalScope()) {
                var2.setValue(false);
                assert var1.getValue() == null;
                assert var2.getValue() == false;
            }

            // Since the transient repo has not been flushed yet, outside the scope
            // the variables should be unchanged.
            assert var1.getValue() == null;
            assert var2.getValue() == null;
        }

        // After finalizing the execution scope, the local scope changes have been flushed.
        assert var1.getValue() == true;
        assert var2.getValue() == false;
    }
}