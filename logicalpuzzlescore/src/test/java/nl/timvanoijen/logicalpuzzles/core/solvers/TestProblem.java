package nl.timvanoijen.logicalpuzzles.core.solvers;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.core.conditions.Condition;
import nl.timvanoijen.logicalpuzzles.core.conditions.XOrCondition;
import nl.timvanoijen.logicalpuzzles.core.keys.StringKey;
import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;

import java.util.*;


public class TestProblem {

    @Getter private final Problem problem;
    @Getter private final ProblemRepository problemRepository;
    @Getter private final List<Boolean> expectedVariableOutcomes;
    @Getter private final List<Runnable> continuationSteps;

    public TestProblem(Problem problem,
                       ProblemRepository problemRepository,
                       List<Boolean> expectedVariableOutcomes,
                       List<Runnable> continuationSteps) {
        this.problem = problem;
        this.continuationSteps = continuationSteps;
        this.problemRepository = problemRepository;
        this.expectedVariableOutcomes = expectedVariableOutcomes;
    }

    public static class Factory {

        @Inject private BoolVar.ProviderFactory boolProviderFactory;
        @Inject private XOrCondition.ProviderFactory xOrConditionProviderFactory;

        public TestProblem createDirectSolvable(ProblemRepository problemRepository) {
            // Conditions:
            // xor(a,b)
            // xor(a,b,c)
            // xor(b,c,d)
            //
            // Initial claim:
            // b -> false
            //
            // Reasoning:
            // a -> true (condition 1)
            // c -> false (condition 2)
            // d -> true (condition 3)

            var boolProvider = this.boolProviderFactory.create();

            BoolVar a = problemRepository.createFinalVariable(StringKey.of("a"), boolProvider);
            BoolVar b = problemRepository.createFinalVariable(StringKey.of("b"), boolProvider);
            BoolVar c = problemRepository.createFinalVariable(StringKey.of("c"), boolProvider);
            BoolVar d = problemRepository.createFinalVariable(StringKey.of("d"), boolProvider);

            // Initial claim
            b.setValue(false);

            Condition c1 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(a,b)));
            Condition c2 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(a,b,c)));
            Condition c3 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(b,c,d)));

            List<Boolean> expectedOutcomes = Arrays.asList(true, false, false, true);

            return new TestProblem(problemRepository.getGrandProblem(),
                    problemRepository, expectedOutcomes, Collections.EMPTY_LIST);
        }

        public TestProblem createDirectSolvableContinuation(ProblemRepository problemRepository) {
            // Conditions:
            // xor(a,b)
            // xor(a,b,c)
            // xor(b,c,d,e)
            //
            // Initial claim:
            // b -> false
            //
            // Reasoning after first claim:
            // a -> true (condition 1)
            // c -> false (condition 2)
            //
            // Second claim;
            // d -> false
            //
            // Reasoning after second claim:
            // e -> true (condition 3)

            var boolProvider = this.boolProviderFactory.create();

            BoolVar a = problemRepository.createFinalVariable(StringKey.of("a"), boolProvider);
            BoolVar b = problemRepository.createFinalVariable(StringKey.of("b"), boolProvider);
            BoolVar c = problemRepository.createFinalVariable(StringKey.of("c"), boolProvider);
            BoolVar d = problemRepository.createFinalVariable(StringKey.of("d"), boolProvider);
            BoolVar e = problemRepository.createFinalVariable(StringKey.of("e"), boolProvider);

            // Initial claim
            b.setValue(false);

            Condition c1 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(a,b)));
            Condition c2 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(a,b,c)));
            Condition c3 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(b,c,d,e)));

            List<Boolean> expectedOutcomes = Arrays.asList(true, false, false, false, true);

            // Set continuation step
            List<Runnable> continuationSteps = Lists.newArrayList(() -> d.setValue(false));

            return new TestProblem(problemRepository.getGrandProblem(),
                    problemRepository, expectedOutcomes, continuationSteps);
        }

        public TestProblem createDirectSolvableIncorrect(ProblemRepository problemRepository) {
            // Conditions:
            // xor(a,b)
            // xor(c,d)
            // xor(a,b,c,d)
            //
            // Initial claim:
            // b -> false
            //
            // Reasoning:
            // a -> true (condition 1)
            // c,d -> false (condition 3)
            // error -> (condition 2)
            var boolProvider = this.boolProviderFactory.create();

            BoolVar a = problemRepository.createFinalVariable(StringKey.of("a"), boolProvider);
            BoolVar b = problemRepository.createFinalVariable(StringKey.of("b"), boolProvider);
            BoolVar c = problemRepository.createFinalVariable(StringKey.of("c"), boolProvider);
            BoolVar d = problemRepository.createFinalVariable(StringKey.of("d"), boolProvider);

            // Initial claim
            b.setValue(false);

            Condition c1 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(a,b)));
            Condition c2 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(c,d)));
            Condition c3 = problemRepository.createCondition(this.xOrConditionProviderFactory.create(Arrays.asList(a,b,c,d)));

            return new TestProblem(problemRepository.getGrandProblem(),
                    problemRepository,null, Collections.EMPTY_LIST);
        }
    }
}
