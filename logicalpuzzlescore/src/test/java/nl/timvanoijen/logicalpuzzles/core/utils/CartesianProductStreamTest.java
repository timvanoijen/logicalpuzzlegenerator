package nl.timvanoijen.logicalpuzzles.core.utils;

import com.google.common.collect.Lists;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class CartesianProductStreamTest {

    @Test
    void of_2D_stream() {
        var rng1 = IntStream.range(0, 2).boxed();
        var rng2 = IntStream.range(2, 4).boxed();
        var pairs = CartesianProductStream.of(rng1, rng2).collect(Collectors.toList());
        assert pairs.equals(
                Arrays.asList(
                        new Pair<>(0,2),
                        new Pair<>(0,3),
                        new Pair<>(1,2),
                        new Pair<>(1,3)
                ));
    }

    @Test
    void of_2D_iterable() {
        var rng1 = Lists.newArrayList(0, 1);
        var rng2 = Lists.newArrayList(2, 3);
        var pairs = CartesianProductStream.of(rng1, rng2).collect(Collectors.toList());
        assert pairs.equals(
                Arrays.asList(
                        new Pair<>(0,2),
                        new Pair<>(0,3),
                        new Pair<>(1,2),
                        new Pair<>(1,3)
                ));
    }

    @Test
    void of_3D_stream() {
        var rng1 = IntStream.range(0, 2).boxed();
        var rng2 = IntStream.range(2, 4).boxed();
        var rng3 = IntStream.range(4, 6).boxed();

        var pairs = CartesianProductStream.of(rng1, rng2, rng3).collect(Collectors.toList());
        assert pairs.equals(
                Arrays.asList(
                        new Triplet<>(0,2,4),
                        new Triplet<>(0,2,5),
                        new Triplet<>(0,3,4),
                        new Triplet<>(0,3,5),
                        new Triplet<>(1,2,4),
                        new Triplet<>(1,2,5),
                        new Triplet<>(1,3,4),
                        new Triplet<>(1,3,5)
                ));
    }

    @Test
    void of_3D_iterable() {
        var rng1 = Lists.newArrayList(0, 1);
        var rng2 = Lists.newArrayList(2, 3);
        var rng3 = Lists.newArrayList(4, 5);
        var pairs = CartesianProductStream.of(rng1, rng2, rng3).collect(Collectors.toList());
        assert pairs.equals(
                Arrays.asList(
                        new Triplet<>(0,2,4),
                        new Triplet<>(0,2,5),
                        new Triplet<>(0,3,4),
                        new Triplet<>(0,3,5),
                        new Triplet<>(1,2,4),
                        new Triplet<>(1,2,5),
                        new Triplet<>(1,3,4),
                        new Triplet<>(1,3,5)
                ));
    }
}