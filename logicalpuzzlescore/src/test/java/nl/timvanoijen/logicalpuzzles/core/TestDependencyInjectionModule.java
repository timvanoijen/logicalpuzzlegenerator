package nl.timvanoijen.logicalpuzzles.core;

import nl.timvanoijen.logicalpuzzles.core.conditions.ConditionTestCase;
import nl.timvanoijen.logicalpuzzles.core.solvers.TestProblem;

public class TestDependencyInjectionModule extends DefaultDependencyInjectionModule {
    @Override
    protected void configure() {
        super.configure();

        // Bind test services
        bind(ConditionTestCase.Factory.class);
        bind(TestProblem.Factory.class);
    }
}
