package nl.timvanoijen.logicalpuzzles.core.keys;

import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.*;
import org.junit.jupiter.api.Test;

public class KeysTest {

    @Test
    void testKeyBuilderSingleKeySource() {
        Key combined = KeyBuilder
                .from(StringKey.of("s1"))
                .with(RowProperty.of(2))
                .with(ColumnProperty.of(1))
                .with(OptionProperty.of(3))
                .build();
        assert combined.asString().equals("{s1:{{col=1}_&_{option=3}_&_{row=2}}}");
    }

    @Test
    void testKeyBuilderKeyIterableOneLevel() {
        var combined = KeyBuilder
                .from(StringKey.of("s1"))
                .fork(ColumnProperty.range(1,3))
                .with(RowProperty.of(2))
                .with(OptionProperty.of(3))
                .build();

        assert combined.size() == 3;
        assert combined.get(0).asString().equals("{s1:{{col=1}_&_{option=3}_&_{row=2}}}");
        assert combined.get(1).asString().equals("{s1:{{col=2}_&_{option=3}_&_{row=2}}}");
        assert combined.get(2).asString().equals("{s1:{{col=3}_&_{option=3}_&_{row=2}}}");
    }

    @Test
    void testKeyBuilderKeyIterableTwoLevels() {

        var combined = KeyBuilder
                .from(StringKey.of("s1"))
                .fork(RowProperty.range(1,2))
                .fork(ColumnProperty.range(1,2))
                .with(OptionProperty.of(3))
                .build();

        assert combined.size() == 2;
        assert combined.get(0).size() == 2;
        assert combined.get(1).size() == 2;

        assert combined.get(0).get(0).asString().equals("{s1:{{col=1}_&_{option=3}_&_{row=1}}}");
        assert combined.get(0).get(1).asString().equals("{s1:{{col=1}_&_{option=3}_&_{row=2}}}");
        assert combined.get(1).get(0).asString().equals("{s1:{{col=2}_&_{option=3}_&_{row=1}}}");
        assert combined.get(1).get(1).asString().equals("{s1:{{col=2}_&_{option=3}_&_{row=2}}}");
    }

    @Test
    void testContinuation() {
        var root = KeyBuilder.create()
                .with(StringProperty.of("p1","a1"))
                .with(StringProperty.of("p2","b1")).build();
        var continuation = KeyBuilder.from(root)
                .with(StringProperty.of("p3","a2"))
                .with(StringProperty.of("p4","b2")).build();

        var expected = KeyBuilder.create()
                .with(StringProperty.of("p2","b1"))
                .with(StringProperty.of("p1","a1"))
                .with(StringProperty.of("p4","b2"))
                .with(StringProperty.of("p3","a2")).build();

        assert continuation.asString().equals(expected.asString());
    }

    @Test
    void testGridKeysGenerator() {
        int width = 5;
        var gridKeysGenerator = new GridKeysGenerator(StringKey.of("cell"), width, width);

        var key = gridKeysGenerator.element(2, 5);
        assert key.get(ColumnProperty.extract()) == 2;
        assert key.get(RowProperty.extract()) == 5;
    }
}
