package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;
import org.junit.jupiter.api.RepeatedTest;

import java.util.List;
import java.util.Random;

public class FalseConditionTest extends TestBase {

    @Inject FalseCondition.ProviderFactory falseConditionProviderFactory;
    @Inject private ConditionTestCase.Factory testCaseFactory;

    private final ProblemRepository problemRepository = new ProblemRepository();
    private static final Random random = new Random(FalseConditionTest.class.getSimpleName().hashCode());

    @RepeatedTest(5)
    public void testTrueConditionFillWithFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {null})
                .setConditionBuilder(new ConditionBuilder())
                .withVarAssert(0, false)
                .execute();
    }

    @RepeatedTest(5)
    public void testTrueConditionError() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true})
                .setConditionBuilder(new ConditionBuilder())
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionFillWithTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {null})
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(false)
                .withVarAssert(0, true)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionError() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {false})
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(false)
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomesTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {false})
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(null)
                .setConditionValueAssert(true)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomesFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true})
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(null)
                .setConditionValueAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionStaysUndetermined() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {null})
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(null)
                .setConditionValueIsNullAssert(true)
                .execute();
    }

    private class ConditionBuilder implements ConditionTestCase.ConditionBuilder {
        public Condition build(Boolean value, List<Variable> vars) {
            return problemRepository.createCondition(
                    falseConditionProviderFactory.create((BoolVar)vars.get(0))
                            .withDefault(value));
        }
    }
}
