package nl.timvanoijen.logicalpuzzles.core.problems.providers;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.keys.*;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.OptionProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.solvers.DirectSolvingStrategy;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingContext;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingResult;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UniqueValuesConditionProviderTest extends TestBase {

    @Inject private IntVar.ProviderFactory intProviderFactory;
    @Inject private DirectSolvingStrategy.Factory directSolvingStrategyFactory;
    @Inject private UniqueValuesConditionProvider.Factory uniqueValuesConditionProviderFactory;
    @Inject private SolvingContext.Factory solvingContextFactory;

    @Test
    void testUniqueValuesConditionGeneratorFullDeduction() {
        Integer[][] input = {
                { 0   , 0   , null, null, null },
                { 0   , null, null, 0   , 0    },
                { null, 0   , 0   , 0   , 0    },
                { null, null, 0   , 0   , 0    },
                { null, null, 0   , 0   , null }
        };

        List<Integer> expectedResult = Arrays.asList(4,3,1,2,5);

        this.innerTestUniqueValuesConditionGenerator(input, expectedResult);
    }

    @Test
    void testUniqueValuesConditionGeneratorVerticalDeduction() {
        Integer[][] input = {
                { 0   , 0   , 0   , null, null },
                { 0   , 0   , null, null, null },
                { 0   , null, null, null, null },
                { null, null, 0   , null, null },
                { 0   , 0   , 0   , null, null }
        };

        List<Integer> expectedResult = Arrays.asList(null,3,2,1,null);

        this.innerTestUniqueValuesConditionGenerator(input, expectedResult);
    }

    @Test
    void testUniqueValuesConditionGeneratorHorizontalDeduction() {
        Integer[][] input = {
                { 0   , 0   , 0   , null, 0 },
                { 0   , 0   , null, null, 0 },
                { 0   , null, null, 0   , 0 },
                { null, null, null, null, null },
                { null, null, null, null, null }
        };

        List<Integer> expectedResult = Arrays.asList(4,3,2,null,null);

        this.innerTestUniqueValuesConditionGenerator(input, expectedResult);
    }

    void innerTestUniqueValuesConditionGenerator(Integer[][] input, List<Integer> expectedResult) {
        var problemRepository = new ProblemRepository();

        var varKeys = KeyBuilder
                .forkFrom(StringKey.range("var_%d", 1, 5))
                .build();

        // Generate variables
        problemRepository.createFinalVariables(varKeys, this.intProviderFactory
                .create().withRange(new IntVar.Range(1,5)));

        // Generate unique values condition
        problemRepository.extend(this.uniqueValuesConditionProviderFactory.create(varKeys));

        // Solve
        this.convertInputToOptionValue(input, problemRepository);
        try(SolvingContext solvingContext = this.solvingContextFactory.create(problemRepository)) {

            var solvingStrategy = this.directSolvingStrategyFactory.create();
            SolvingResult result = solvingContext.solve(
                    problemRepository.getGrandProblem(), solvingStrategy);

            var actualResult = problemRepository.getVariables(varKeys)
                    .stream()
                    .map(i -> i.getValue()).collect(Collectors.toList());
            assert actualResult.equals(expectedResult);
        }
    }

    private void convertInputToOptionValue(Integer[][] input, ProblemRepository problemRepository) {
        for(var i = 0; i < input.length; i++) {
            for(var j = 0; j < input[i].length; j++) {
                if (input[i][j] == null)
                    continue;
                Key optionKey = KeyBuilder.from(StringKey.of(String.format("var_%d",i+1)))
                        .with(OptionProperty.of(j+1)).build();
                problemRepository.<BoolVar>getVariable(optionKey).setValue(input[i][j] == 1);
            }
        }
    }
}