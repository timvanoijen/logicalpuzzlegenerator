package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;
import org.junit.jupiter.api.RepeatedTest;

import java.util.List;
import java.util.Random;

public class AndConditionTest extends TestBase {

    @Inject AndCondition.ProviderFactory andConditionProviderFactory;
    @Inject private ConditionTestCase.Factory testCaseFactory;

    private final ProblemRepository problemRepository = new ProblemRepository();
    private static final Random random = new Random(AndConditionTest.class.getSimpleName().hashCode());

    @RepeatedTest(5)
    public void testTrueConditionFillWithTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, true, null, null })
                .setConditionBuilder(new ConditionBuilder())
                .withVarAssert(2, true)
                .withVarAssert(3, true)
                .execute();
    }

    @RepeatedTest(5)
    public void testTrueConditionError() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {false, true, null, null })
                .setConditionBuilder(new ConditionBuilder())
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionFillWithFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, true, null })
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(false)
                .withVarAssert(2, false)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionStayUndetermined() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, false, null })
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(false)
                .withVarAssert(2,null)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionError() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, true, true })
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(false)
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomeTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, true, true })
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(null)
                .setConditionValueAssert(true)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomeFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, false, null })
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(null)
                .setConditionValueAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionStayUndetermined() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[] {true, null, null })
                .setConditionBuilder(new ConditionBuilder())
                .setConditionValue(null)
                .setConditionValueIsNullAssert(true)
                .execute();
    }

    private class ConditionBuilder implements ConditionTestCase.ConditionBuilder {
        public Condition build(Boolean value, List<Variable> vars) {
            return problemRepository.createCondition(
                    andConditionProviderFactory.create((List<BoolVar>)(List)vars).withDefault(value));
        }
    }
}
