package nl.timvanoijen.logicalpuzzles.core.conditions;

import com.google.inject.Inject;
import lombok.Setter;
import lombok.experimental.Accessors;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;
import org.junit.jupiter.api.RepeatedTest;

import java.util.*;

public class CountTrueConditionTest extends TestBase {

    @Inject
    private CountTrueCondition.ProviderFactory countTrueConditionProviderFactory;
    @Inject
    private ConditionTestCase.Factory testCaseFactory;

    private final ProblemRepository problemRepository = new ProblemRepository();
    private static final Random random = new Random(CountTrueConditionTest.class.getSimpleName().hashCode());

    @RepeatedTest(5)
    public void testTrueConditionFillWithTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, true, false, false, null, null})
                .setConditionBuilder(new ConditionBuilder().setMinTrue(5).setMaxTrue(7))
                .withVarAssert(5, true)
                .withVarAssert(6, true)
                .execute();
    }

    @RepeatedTest(5)
    public void testTrueConditionFillWithFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, true, false, false, null, null})
                .setConditionBuilder(new ConditionBuilder().setMinTrue(2).setMaxTrue(3))
                .withVarAssert(5, false)
                .withVarAssert(6, false)
                .execute();
    }

    @RepeatedTest(5)
    public void testTrueConditionUndetermined() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, false, null, null})
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(3))
                .withVarAssert(5, null)
                .withVarAssert(6, null)
                .execute();
    }

    @RepeatedTest(5)
    public void testTrueConditionWithError1() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, false, null, null})
                .setConditionBuilder(new ConditionBuilder().setMinTrue(5).setMaxTrue(6))
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testTrueConditionWithError2() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, false, null, null})
                .setConditionBuilder(new ConditionBuilder().setMinTrue(0).setMaxTrue(1))
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionFillTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, false, null, null})
                .setConditionValue(false)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(1).setMaxTrue(3))
                .withVarAssert(5, true)
                .withVarAssert(6, true)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionFillFalse() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, false, null, null})
                .setConditionValue(false)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(4))
                .withVarAssert(5, false)
                .withVarAssert(6, false)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionUndetermined1() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, false, null, null})
                .setConditionValue(false)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(3))
                .withVarAssert(5, null)
                .withVarAssert(6, null)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionUndetermined2() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, false, false, false, null, null, null})
                .setConditionValue(false)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(6))
                .withVarAssert(5, null)
                .withVarAssert(6, null)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionUndetermined3() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, true, true, null, null, null})
                .setConditionValue(false)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(5))
                .withVarAssert(5, null)
                .withVarAssert(6, null)
                .execute();
    }

    @RepeatedTest(5)
    public void testFalseConditionError() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, true, false, false, null, null})
                .setConditionValue(false)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(5))
                .setResultAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomeTrue() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, true, false, false, null, null})
                .setConditionValue(null)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(5))
                .setConditionValueAssert(true)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomeFalse1() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, null, null, null})
                .setConditionValue(null)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(6).setMaxTrue(7))
                .setConditionValueAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionBecomeFalse2() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, null, null, null})
                .setConditionValue(null)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(0).setMaxTrue(1))
                .setConditionValueAssert(false)
                .execute();
    }

    @RepeatedTest(5)
    public void testUndeterminedConditionStayUndetermined() {
        testCaseFactory.create(random, this.problemRepository)
                .setUpdates(new Boolean[]{true, true, false, false, null, null, null})
                .setConditionValue(null)
                .setConditionBuilder(new ConditionBuilder().setMinTrue(3).setMaxTrue(5))
                .setConditionValueIsNullAssert(true)
                .execute();
    }

    @Accessors(chain = true)
    private class ConditionBuilder implements ConditionTestCase.ConditionBuilder {
        @Setter
        private int minTrue;
        @Setter
        private int maxTrue;

        public Condition build(Boolean value, List<Variable> vars) {
            return problemRepository.createCondition(
                    countTrueConditionProviderFactory
                            .create((List<BoolVar>)(List)vars)
                              .withDefault(value)
                              .withMinTrue(this.minTrue).withMaxTrue(this.maxTrue));
        }
    }
}
