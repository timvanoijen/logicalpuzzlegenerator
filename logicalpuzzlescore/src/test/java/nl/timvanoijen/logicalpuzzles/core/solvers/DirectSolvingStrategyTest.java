package nl.timvanoijen.logicalpuzzles.core.solvers;

import com.google.common.collect.Streams;
import com.google.inject.Inject;
import nl.timvanoijen.logicalpuzzles.core.TestBase;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class DirectSolvingStrategyTest extends TestBase {

    @Inject DirectSolvingStrategy.Factory solvingStrategyFactory;
    @Inject TestProblem.Factory testPuzzleFactory;
    @Inject SolvingContext.Factory solvingContextFactory;

    @Test
    void testSuccessfulSolving() {

        var problemRepository = new ProblemRepository();
        var testProblem = this.testPuzzleFactory.createDirectSolvable(problemRepository);
        var solvingContext = solvingContextFactory.create(problemRepository);
        var solvingStrategy = this.solvingStrategyFactory.create();

        assert solvingContext.solve(testProblem.getProblem(), solvingStrategy).isSucceeded();

        List<Boolean> expectedValues = testProblem.getExpectedVariableOutcomes();
        List<Object> actualValues = new ArrayList<>();
        Streams.stream(testProblem.getProblem().getVariables())
                .filter(v -> v instanceof BoolVar)
                .forEach(b -> actualValues.add(b.getValue()));

        for(int i = 0; i < expectedValues.size(); i++)
            assert Objects.equals(expectedValues.get(i), actualValues.get(i));
    }

    @Test
    void testFailureSolving() {
        var problemRepository = new ProblemRepository();
        var testProblem = this.testPuzzleFactory.createDirectSolvableIncorrect(problemRepository);
        var solvingContext = solvingContextFactory.create(problemRepository);
        var solvingStrategy = this.solvingStrategyFactory.create();

        assert solvingContext.solve(testProblem.getProblem(), solvingStrategy).isFailed();
    }
}
