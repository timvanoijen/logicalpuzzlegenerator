package nl.timvanoijen.logicalpuzzles.logicalpuzzlesweb;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import nl.timvanoijen.logicalpuzzles.logicalpuzzlesweb.puzzlerequesthandlers.NumberSquareRequestHandler;
import nl.timvanoijen.logicalpuzzles.logicalpuzzlesweb.puzzlerequesthandlers.PuzzleRequestHandler;

@WebServlet(name = "generator", value = "/generate/*")
public class RequestHandler extends HttpServlet {
    private Map<String, PuzzleRequestHandler> handlersByType = new HashMap<>();


    public void init() {
        this.handlersByType.put("numbersquare", new NumberSquareRequestHandler());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        var pathParts = request.getPathInfo().split("/");
        var puzzleType= pathParts[1];
        PuzzleRequestHandler handler = this.handlersByType.get(puzzleType);
        String result = handler.handleGenerateRequest(request);
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result);
    }

    public void destroy() {
    }
}