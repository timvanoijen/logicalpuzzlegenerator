package nl.timvanoijen.logicalpuzzles.logicalpuzzlesweb.puzzlerequesthandlers;

import jakarta.servlet.http.HttpServletRequest;

public interface PuzzleRequestHandler {
    String handleGenerateRequest(HttpServletRequest request);
}
