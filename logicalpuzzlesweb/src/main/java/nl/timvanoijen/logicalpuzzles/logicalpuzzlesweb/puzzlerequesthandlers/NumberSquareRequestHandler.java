package nl.timvanoijen.logicalpuzzles.logicalpuzzlesweb.puzzlerequesthandlers;

import com.google.inject.Guice;
import jakarta.servlet.http.HttpServletRequest;
import nl.timvanoijen.logicalpuzzles.numbersquare.NumberSquareDependencyInjectionModule;
import nl.timvanoijen.logicalpuzzles.numbersquare.services.NumberSquareService;


public class NumberSquareRequestHandler implements PuzzleRequestHandler {
    private NumberSquareService service;

    public NumberSquareRequestHandler() {
        this.service = Guice.createInjector(new NumberSquareDependencyInjectionModule()).getInstance(NumberSquareService.class);
    }

    public String handleGenerateRequest(HttpServletRequest request) {
        int width = Integer.parseInt(request.getParameter("width")); // TODO: exception handling
        var puzzle = this.service.generate(width);
        return puzzle.getInitialPuzzle().toString();
    }
}
