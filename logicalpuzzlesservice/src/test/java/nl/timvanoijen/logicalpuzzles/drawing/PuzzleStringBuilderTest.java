package nl.timvanoijen.logicalpuzzles.drawing;

import org.junit.jupiter.api.Test;


class PuzzleStringBuilderTest {

    @Test
    void testArtiFarti() {
        var pb = new PuzzleStringBuilder();
        pb.addCell(-1, -1, "L");
        pb.addRectangle(-1, -1, 2, 3, LineStyle.SolidHeavy);
        pb.addCell(0, 0, "H");
        pb.addRectangle(0,0,2, 3, LineStyle.SolidLight);
        var result = pb.build(new UtfElementDrawer());
        var expected =
        "╔═══════╗\n" +
        "║ L     ║\n" +
        "║   ┌───╫───┐\n" +
        "║   │ H ║   │\n" +
        "║   │   ║   │\n" +
        "║   │   ║   │\n" +
        "╚═══╪═══╝   │\n" +
        "    │       │\n" +
        "    └───────┘";

        assert result.equals(expected);
    }

    @Test
    void testGrid() {
        var pb = new PuzzleStringBuilder();

        for(var x = 0; x < 4; x++) {
            for (var y = 0; y < 4; y++) {
                pb.addRectangle(x, y, 1, 1, LineStyle.SolidLight);
                pb.addCell(x, y, Integer.toString(x + y));
            }
        }

        for(var x = 0; x < 4; x+=2)
            for(var y = 0; y < 4; y+=2)
                pb.addRectangle(x, y,2,2, LineStyle.SolidHeavy);

        var result = pb.build(new UtfElementDrawer());
        var expected =
                "╔═══╤═══╦═══╤═══╗\n" +
                "║ 0 │ 1 ║ 2 │ 3 ║\n" +
                "╟───┼───╫───┼───╢\n" +
                "║ 1 │ 2 ║ 3 │ 4 ║\n" +
                "╠═══╪═══╬═══╪═══╣\n" +
                "║ 2 │ 3 ║ 4 │ 5 ║\n" +
                "╟───┼───╫───┼───╢\n" +
                "║ 3 │ 4 ║ 5 │ 6 ║\n" +
                "╚═══╧═══╩═══╧═══╝";

        assert result.equals(expected);
    }

    @Test
    void testPaddingEnContentSize() {
        var pb = new PuzzleStringBuilder()
                .withCellContentSize(new Size(2,1))
                .withCellPadding(new Size(0,1));
        pb.addCell(3, 3, "X");
        pb.addRectangle(3, 3, 1, 1, LineStyle.SolidHeavy);
        pb.addCell(4, 3, "YY");
        pb.addRectangle(4, 3, 1, 1, LineStyle.SolidLight);
        pb.addRectangle(4, 3, 2, 1, LineStyle.SolidHeavy);

        var result = pb.build(new UtfElementDrawer());
        var expected =
                "╔══╦══╤══╗\n" +
                "║  ║  │  ║\n" +
                "║ X║YY│  ║\n" +
                "║  ║  │  ║\n" +
                "╚══╩══╧══╝";

        assert result.equals(expected);
    }
}