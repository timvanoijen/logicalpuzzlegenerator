package nl.timvanoijen.logicalpuzzles.drawing;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@Getter
@AllArgsConstructor
public final class Point {
    private int x;
    private int y;

    static final Point ORIGIN = new Point(0,0);

    public Point plus(Point other) {
        return new Point(x + other.x, y + other.y);
    }

    public Point plus(Size s) {
        return new Point(x + s.getHorizontal(), y + s.getVertical());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.x, this.y);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Point))
            return false;
        Point p = (Point) o;
        return p.x == this.x && p.y == this.y;
    }
}
