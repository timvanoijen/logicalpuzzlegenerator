package nl.timvanoijen.logicalpuzzles.drawing;

public enum LineStyle {
    None,
    SolidLight,
    SolidHeavy
}
