package nl.timvanoijen.logicalpuzzles.drawing;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@Getter
@AllArgsConstructor
public class Size {
    private final int horizontal;
    private final int vertical;

    @Override
    public int hashCode() {
        return Objects.hash(this.horizontal, this.vertical);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Size))
            return false;
        Size s = (Size) o;
        return s.horizontal == this.horizontal &&
                s.vertical == this.vertical;
    }
}
