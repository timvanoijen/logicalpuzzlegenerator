package nl.timvanoijen.logicalpuzzles.drawing;


import java.util.*;

public class PuzzleStringBuilder {

    private final Map<Point, String> cells = new HashMap<>();
    private final Map<SquareSide, LineStyle> lines = new HashMap<>();
    private final Map<SquareCorner, CornerStyle> corners = new HashMap<>();

    private Size cellContentSize = new Size(1, 1);
    private Size cellPadding = new Size(1,0);

    public PuzzleStringBuilder withCellContentSize(Size s) {
        this.cellContentSize = s;
        return this;
    }

    public PuzzleStringBuilder withCellPadding(Size s) {
        this.cellPadding = s;
        return this;
    }

    public PuzzleStringBuilder addCell(int x, int y, String content) {
        this.cells.put(new Point(x, y), content);
        return this;
    }

    public PuzzleStringBuilder addLine(int x, int y, Side side, LineStyle style) {
        lines.put(new SquareSide(new Point(x,y), side), style);
        return this;
    }

    public PuzzleStringBuilder addCorner(int x, int y, Corner corner, CornerStyle style) {
        corners.put(new SquareCorner(new Point(x,y), corner), style);
        return this;
    }

    public PuzzleStringBuilder addRectangle(int left, int top, int width, int height, LineStyle style) {
        for(var i = 0; i < width; i++) {
            this.addLine(left + i, top, Side.Top, style);
            this.addLine(left + i, top + height - 1, Side.Bottom, style);
        }
        for(var i = 0; i < height; i++) {
            this.addLine(left, top + i, Side.Left, style);
            this.addLine(left + width - 1, top + i, Side.Right, style);
        }
        return this;
    }

    public String build(ElementDrawer elementDrawer) {
        Point offset = this.calcOffset();

        List<List<String>> matrix = new ArrayList<>();

        // Loop through center points
        for(var cell : this.cells.keySet()) {
            var rect = this.squareCenterToRenderRectangle(cell, offset);
            var value = this.cells.get(cell);
            var relOffset = this.cellContentSize.getHorizontal() - value.length();
            for(var i = 0; i < value.length(); i++) {
                Point charPos = new Point(
                        rect.getTopLeft().getX() + i + relOffset,
                        rect.getTopLeft().getY());
                this.setStringMatrixValue(matrix, charPos,
                        String.valueOf(value.charAt(i)));
            }
        }

        // Loop through sides
        for(var line : this.lines.keySet()) {
            var lineStyle = this.lines.get(line);
            var lineStart = this.squareCornerToRenderPoint(line.getCenter(),
                    line.getSide().getStart(), offset);
            var lineEnd = this.squareCornerToRenderPoint(line.getCenter(),
                    line.getSide().getEnd(), offset);

            if (lineStart.getX() == lineEnd.getX()) {
                for(var y = lineStart.getY() + 1; y < lineEnd.getY(); y++) {
                    this.setStringMatrixValue(matrix, new Point(lineStart.getX(), y),
                            elementDrawer.drawSide(line.getSide(), lineStyle));
                }
            } else {
                for (var x = lineStart.getX() + 1; x < lineEnd.getX(); x++) {
                    this.setStringMatrixValue(matrix, new Point(x, lineStart.getY()),
                            elementDrawer.drawSide(line.getSide(), lineStyle));
                }
            }
        }

        // Loop through corners (explicitly given or generated from sides)
        var allSquareCorners = new HashSet<>(this.corners.keySet());
        for(var line : this.lines.keySet()) {
            allSquareCorners.add(new SquareCorner(line.getCenter(), line.getSide().getStart()));
            allSquareCorners.add(new SquareCorner(line.getCenter(), line.getSide().getEnd()));
        }

        for(var sc : allSquareCorners) {
            var p = this.squareCornerToRenderPoint(sc.getCenter(),
                    sc.getCorner(), offset);
            String value;
            if (this.corners.containsKey(sc))
                value = elementDrawer.drawCorner(sc.getCorner(), this.corners.get(sc));
            else {
                // Determine corner style from sides
                LineStyle top = this.lines.getOrDefault(sc.getTopSide(), LineStyle.None);
                LineStyle right = this.lines.getOrDefault(sc.getRightSide(), LineStyle.None);
                LineStyle bottom = this.lines.getOrDefault(sc.getBottomSide(), LineStyle.None);
                LineStyle left = this.lines.getOrDefault(sc.getLeftSide(), LineStyle.None);
                value = elementDrawer.drawCornerFromSides(top, right, bottom, left);
            }
            this.setStringMatrixValue(matrix, p, value);
        }

        // Generate final string
        List<String> lines = new ArrayList<>();
        for(var row : matrix)
            lines.add(String.join("", row));
        return String.join("\n", lines);
    }

    private Point calcOffset() {
        List<Point> allPoints = new ArrayList<>();

        // Loop through center points
        for(var square : this.cells.keySet()) {
            var rect = this.squareCenterToRenderRectangle(square, Point.ORIGIN);
            allPoints.addAll(rect.getCorners());
        }

        // Loop through sides
        for(var line : this.lines.keySet()) {
            allPoints.add(this.squareCornerToRenderPoint(line.getCenter(),
                    line.getSide().getStart(), Point.ORIGIN));
            allPoints.add(this.squareCornerToRenderPoint(line.getCenter(),
                    line.getSide().getEnd(), Point.ORIGIN));
        }

        // Loop through points
        for(var corner : this.corners.keySet())
            allPoints.add(this.squareCornerToRenderPoint(corner.getCenter(),
                    corner.getCorner(), Point.ORIGIN));

        // Determine offsets
        int offsetX = Integer.MIN_VALUE;
        int offsetY = Integer.MIN_VALUE;
        for(Point p : allPoints) {
            offsetX = Integer.max(offsetX, -p.getX());
            offsetY = Integer.max(offsetY, -p.getY());
        }

        return new Point(offsetX, offsetY);
    }

    private Rectangle squareCenterToRenderRectangle(Point square, Point offset) {
        int squareWidth = (1 + this.cellContentSize.getHorizontal() +
                2 * this.cellPadding.getHorizontal());
        int renderX = squareWidth*square.getX() + 1 + this.cellPadding.getHorizontal();
        int squareHeight = (1 + this.cellContentSize.getVertical() + 2 * this.cellPadding.getVertical());
        int renderY = squareHeight*square.getY() + 1 + this.cellPadding.getVertical();
        return new Rectangle(new Point(renderX, renderY).plus(offset), this.cellContentSize);
    }

    private Point squareCornerToRenderPoint(Point square, Corner corner, Point offset) {
        Rectangle centerRectangle = this.squareCenterToRenderRectangle(square, offset);

        if (corner == Corner.TopLeft) {
            return centerRectangle.getTopLeft().plus(new Point(
                    -1 - this.cellPadding.getHorizontal(),
                    -1 - this.cellPadding.getVertical()));
        }
        if (corner == Corner.TopRight) {
            return centerRectangle.getTopRight().plus(new Point(
                    1 + this.cellPadding.getHorizontal(),
                    -1 - this.cellPadding.getVertical()));
        }
        if (corner == Corner.BottomRight) {
            return centerRectangle.getBottomRight().plus(new Point(
                    1 + this.cellPadding.getHorizontal(),
                    1 + this.cellPadding.getVertical()));
        }
       //BottomLeft
       return centerRectangle.getBottomLeft().plus(new Point(
                    -1 - this.cellPadding.getHorizontal(),
                    1 + this.cellPadding.getVertical()));
    }

    private void setStringMatrixValue(List<List<String>> matrix, Point p, String s) {
        while(matrix.size() <= p.getY())
            matrix.add(new ArrayList<>());
        var lst = matrix.get(p.getY());
        while(lst.size() <= p.getX())
            lst.add(" ");
        lst.set(p.getX(), s);
    }
}
