package nl.timvanoijen.logicalpuzzles.drawing;

public enum CornerStyle {
    None,
    Cross,
    Dot
}
