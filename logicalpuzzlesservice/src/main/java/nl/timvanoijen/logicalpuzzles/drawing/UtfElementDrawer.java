package nl.timvanoijen.logicalpuzzles.drawing;

import org.javatuples.Quartet;

import java.util.HashMap;
import java.util.Map;

public class UtfElementDrawer implements ElementDrawer {

    private final Map<Quartet<LineStyle, LineStyle, LineStyle, LineStyle>, String> sidesToCornerMap;

    public UtfElementDrawer() {
        this.sidesToCornerMap = buildCornerFromSidesMap();
    }

    @Override
    public String drawSide(Side side, LineStyle style) {
        if (side == Side.Left || side == Side.Right) {
            if (style == LineStyle.SolidLight)
                return "│";
            else if (style == LineStyle.SolidHeavy)
                return "║";
        } else {
            if (style == LineStyle.SolidLight)
                return "─";
            else if (style == LineStyle.SolidHeavy)
                return "═";
        }
        return " ";
    }

    @Override
    public String drawCorner(Corner corner, CornerStyle style) {
        if (style == CornerStyle.Cross)
            return "┽";
        else if (style == CornerStyle.Dot)
            return "⬤";
        return " ";
    }

    @Override
    public String drawCornerFromSides(LineStyle top, LineStyle right, LineStyle bottom, LineStyle left) {
        var key = new Quartet<>(top, right, bottom, left);
        return this.sidesToCornerMap.get(key);
    }

    private static Map<Quartet<LineStyle, LineStyle, LineStyle, LineStyle>, String> buildCornerFromSidesMap() {
        Map<Quartet<LineStyle, LineStyle, LineStyle, LineStyle>, String> c = new HashMap<>();

        // Top None
        add(c, 0,0,0,0, " ");
        add(c, 0,0,0,1, "╴");
        add(c, 0,0,0,2, " ");
        add(c, 0,0,1,0, "╷");
        add(c, 0,0,1,1, "┐");
        add(c, 0,0,1,2, "╕");
        add(c, 0,0,2,0, "╷");
        add(c, 0,0,2,1, "╖");
        add(c, 0,0,2,2, "╗");

        add(c, 0,1,0,0, "╶");
        add(c, 0,1,0,1, "─");
        add(c, 0,1,0,2, "─");
        add(c, 0,1,1,0, "┌");
        add(c, 0,1,1,1, "┬");
        add(c, 0,1,1,2, "┌");
        add(c, 0,1,2,0, "╓");
        add(c, 0,1,2,1, "╥");
        add(c, 0,1,2,2, "╗");

        add(c, 0,2,0,0, " ");
        add(c, 0,2,0,1, "─");
        add(c, 0,2,0,2, "═");
        add(c, 0,2,1,0, "╒");
        add(c, 0,2,1,1, "┐");
        add(c, 0,2,1,2, "╤");
        add(c, 0,2,2,0, "╔");
        add(c, 0,2,2,1, "╔");
        add(c, 0,2,2,2, "╦");

        // Top light
        add(c, 1,0,0,0, "╵");
        add(c, 1,0,0,1, "┘");
        add(c, 1,0,0,2, "╛");
        add(c, 1,0,1,0, "│");
        add(c, 1,0,1,1, "┤");
        add(c, 1,0,1,2, "╡");
        add(c, 1,0,2,0, "│");
        add(c, 1,0,2,1, "┘");
        add(c, 1,0,2,2, "╗");

        add(c, 1,1,0,0, "└");
        add(c, 1,1,0,1, "┴");
        add(c, 1,1,0,2, "└");
        add(c, 1,1,1,0, "├");
        add(c, 1,1,1,1, "┼");
        add(c, 1,1,1,2, "├");
        add(c, 1,1,2,0, "└");
        add(c, 1,1,2,1, "┴");
        add(c, 1,1,2,2, "╗");

        add(c, 1,2,0,0, "╘");
        add(c, 1,2,0,1, "┘");
        add(c, 1,2,0,2, "╧");
        add(c, 1,2,1,0, "╞");
        add(c, 1,2,1,1, "┤");
        add(c, 1,2,1,2, "╪");
        add(c, 1,2,2,0, "╔");
        add(c, 1,2,2,1, "╔");
        add(c, 1,2,2,2, "╦");

        // Top heavy
        add(c, 2,0,0,0, " ");
        add(c, 2,0,0,1, "╜");
        add(c, 2,0,0,2, "╝");
        add(c, 2,0,1,0, "╷");
        add(c, 2,0,1,1, "┐");
        add(c, 2,0,1,2, "╝");
        add(c, 2,0,2,0, "║");
        add(c, 2,0,2,1, "╢");
        add(c, 2,0,2,2, "╣");

        add(c, 2,1,0,0, "╙");
        add(c, 2,1,0,1, "╨");
        add(c, 2,1,0,2, "╝");
        add(c, 2,1,1,0, "┌");
        add(c, 2,1,1,1, "┬");
        add(c, 2,1,1,2, "╝");
        add(c, 2,1,2,0, "╟");
        add(c, 2,1,2,1, "╫");
        add(c, 2,1,2,2, "╣");

        add(c, 2,2,0,0, "╚");
        add(c, 2,2,0,1, "╚");
        add(c, 2,2,0,2, "╩");
        add(c, 2,2,1,0, "╚");
        add(c, 2,2,1,1, "╚");
        add(c, 2,2,1,2, "╩");
        add(c, 2,2,2,0, "╠");
        add(c, 2,2,2,1, "╠");
        add(c, 2,2,2,2, "╬");

        return c;
    }

    private static void add(Map<Quartet<LineStyle, LineStyle, LineStyle, LineStyle>, String> c,
                     int t, int r, int b, int l, String s) {
        LineStyle top = (t == 0) ? LineStyle.None :
                ((t == 1) ? LineStyle.SolidLight : LineStyle.SolidHeavy);
        LineStyle right = (r == 0) ? LineStyle.None :
                ((r == 1) ? LineStyle.SolidLight : LineStyle.SolidHeavy);
        LineStyle bottom = (b == 0) ? LineStyle.None :
                ((b == 1) ? LineStyle.SolidLight : LineStyle.SolidHeavy);
        LineStyle left = (l == 0) ? LineStyle.None :
                ((l == 1) ? LineStyle.SolidLight : LineStyle.SolidHeavy);
        c.put(new Quartet<>(top, right, bottom, left), s);
    }
}
