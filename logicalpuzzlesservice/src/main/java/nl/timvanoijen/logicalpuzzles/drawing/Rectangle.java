package nl.timvanoijen.logicalpuzzles.drawing;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Objects;

@Getter
@AllArgsConstructor
public class Rectangle {
    private final Point topLeft;
    private final Size size;

    public List<Point> getCorners() {
        return Lists.newArrayList(this.getTopLeft(),
                this.getTopRight(),
                this.getBottomRight(),
                this.getBottomLeft());
    }

    public Point getTopLeft() {
        return this.topLeft;
    }

    public Point getTopRight() {
        return this.topLeft.plus(new Point(size.getHorizontal() - 1, 0));
    }

    public Point getBottomRight() {
        return this.topLeft.plus(new Point(size.getHorizontal() -1, size.getVertical() - 1));
    }

    public Point getBottomLeft() {
        return this.topLeft.plus(new Point(0, size.getVertical() - 1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.topLeft, this.size);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Rectangle))
            return false;
        Rectangle r = (Rectangle) o;
        return Objects.equals(r.topLeft, this.topLeft) &&
                Objects.equals(r.size, this.size);
    }
}
