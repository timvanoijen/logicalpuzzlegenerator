package nl.timvanoijen.logicalpuzzles.drawing;

public interface ElementDrawer {
    String drawSide(Side side, LineStyle style);
    String drawCorner(Corner corner, CornerStyle style);
    String drawCornerFromSides(LineStyle top, LineStyle right,
                               LineStyle bottom, LineStyle left);
}
