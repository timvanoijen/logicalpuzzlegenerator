package nl.timvanoijen.logicalpuzzles.drawing;

enum Side {
    Top,
    Right,
    Left,
    Bottom;

    public Corner getStart() {
        if (this == Side.Top)
            return Corner.TopLeft;
        if (this == Side.Right)
            return Corner.TopRight;
        if (this == Side.Bottom)
            return Corner.BottomLeft;
        // Left
        return Corner.TopLeft;
    }

    public Corner getEnd() {
        if (this == Side.Top)
            return Corner.TopRight;
        if (this == Side.Right)
            return Corner.BottomRight;
        if (this == Side.Bottom)
            return Corner.BottomRight;
        // Left
        return Corner.BottomLeft;
    }
}
