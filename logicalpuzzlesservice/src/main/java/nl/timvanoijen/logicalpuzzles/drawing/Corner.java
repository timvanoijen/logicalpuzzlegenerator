package nl.timvanoijen.logicalpuzzles.drawing;

enum Corner {
    TopLeft,
    TopRight,
    BottomRight,
    BottomLeft;

    public Point getOffsetFromCenter() {
        if (this == Corner.TopLeft)
            return new Point(-1, -1);
        if (this == Corner.TopRight)
            return new Point( 1, -1);
        if (this == Corner.BottomRight)
            return new Point( 1, 1);
        // Bottom left
        return new Point( -1, 1);
    }
}
