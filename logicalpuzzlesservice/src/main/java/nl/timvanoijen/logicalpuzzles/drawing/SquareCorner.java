package nl.timvanoijen.logicalpuzzles.drawing;

import lombok.Getter;

import java.util.Objects;

public final class SquareCorner {
    @Getter private final Point center;
    @Getter private final Corner corner;
    private final Point normalizedCenter;

    public SquareCorner(Point center, Corner corner) {
        this.center = center;
        this.corner = corner;
        this.normalizedCenter = getNormalizedCenter(center, corner);
    }

    public SquareSide getTopSide() {
        return new SquareSide(this.normalizedCenter.plus(new Point(0, -1)), Side.Left);
    }

    public SquareSide getRightSide() {
        return new SquareSide(this.normalizedCenter, Side.Top);
    }

    public SquareSide getBottomSide() {
        return new SquareSide(this.normalizedCenter, Side.Left);
    }

    public SquareSide getLeftSide() {
        return new SquareSide(this.normalizedCenter.plus(new Point(-1, 0)), Side.Top);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.normalizedCenter);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof SquareCorner))
            return false;
        SquareCorner s = (SquareCorner)o;
        return Objects.equals(s.normalizedCenter, this.normalizedCenter);
    }

    private static Point getNormalizedCenter(Point center, Corner corner) {
        if (corner == Corner.TopLeft) {
            return center;
        } else if (corner == Corner.TopRight) {
            return center.plus(new Point(1,0));
        } else if (corner == Corner.BottomRight) {
            return center.plus(new Point(1,1));
        } else { // BottomLeft
            return center.plus(new Point(0,1));
        }
    }
}
