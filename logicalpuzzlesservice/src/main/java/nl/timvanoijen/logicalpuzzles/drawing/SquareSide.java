package nl.timvanoijen.logicalpuzzles.drawing;

import lombok.Getter;

import java.util.Objects;

public final class SquareSide {
    @Getter private final Point center;
    @Getter private final Side side;
    private final Point normalizedCenter;
    private final boolean isHorizontal;

    public SquareSide(Point center, Side side) {
        this.center = center;
        this.side = side;
        this.isHorizontal = (side == Side.Top) || (side == Side.Bottom);
        this.normalizedCenter = getNormalizedCenter(center, side);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.normalizedCenter, this.isHorizontal);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof SquareSide))
            return false;
        SquareSide s = (SquareSide) o;
        return Objects.equals(s.normalizedCenter, this.normalizedCenter)
                && s.isHorizontal == this.isHorizontal;
    }

    private static Point getNormalizedCenter(Point center, Side side) {
        if (side == Side.Top || side == Side.Left) {
            return center;
        } else if (side == Side.Bottom) {
            return center.plus(new Point(0,1));
        } else { // Right
            return center.plus(new Point(1,0));
        }
    }

    @Override
    public String toString() {
        return String.format("(%d,%d): %s",
                center.getX(), center.getY(), side);
    }
}
