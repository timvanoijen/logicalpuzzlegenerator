package nl.timvanoijen.logicalpuzzles.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GridEntry {
    private final int column;
    private final int row;
    private final int value;
}
