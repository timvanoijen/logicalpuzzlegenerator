package nl.timvanoijen.logicalpuzzles.service;

import nl.timvanoijen.logicalpuzzles.core.keys.generators.GridKeysGenerator;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.ColumnProperty;
import nl.timvanoijen.logicalpuzzles.core.keys.keyproperties.RowProperty;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.variables.IntVar;
import nl.timvanoijen.logicalpuzzles.models.GridEntry;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GridUtils {

    public static Integer[][] convertGridVariablesTo2DArray(GridKeysGenerator gridKeysGenerator,
                                                            ProblemRepository problemRepository) {
        var height = gridKeysGenerator.getHeight();
        var width = gridKeysGenerator.getWidth();
        var variables = problemRepository
                .<IntVar>getVariables(gridKeysGenerator.elements());
        return convertGridVariablesTo2DArray(variables, width, height);
    }


    public static Integer[][] convertGridVariablesTo2DArray(Iterable<IntVar> variables,
                                                            int width, int height) {
        var result = new Integer[width][height];

        for(var v : variables) {
            var col = v.getKey().get(ColumnProperty.extract());
            var row = v.getKey().get(RowProperty.extract());
            var value = v.getValue();
            result[col - 1][row - 1] = value;
        }
        return result;
    }

    public static void convert2DArrayToGridVariables(Integer[][] values,
                                                     GridKeysGenerator gridKeysGenerator,
                                                     ProblemRepository problemRepository) {
        var height = gridKeysGenerator.getHeight();
        var width =gridKeysGenerator.getWidth();

        for(var row = 0; row < height; row++) {
            for(var col = 0; col < width; col++) {
                problemRepository.<IntVar>getVariable(gridKeysGenerator.element(col + 1,row + 1))
                        .setValue(values[col][row]);
            }
        }
    }

    public static List<GridEntry> convertGridVariablesToSortedGridEntries(
            GridKeysGenerator gridKeysGenerator, ProblemRepository problemRepository) {
        var sortedVars = problemRepository
                .<IntVar>getVariables(gridKeysGenerator.elements()).stream()
                .sorted(Comparator.comparing(v -> v.getLastModified()));

        return sortedVars.map(v -> new GridEntry(
                v.getKey().get(ColumnProperty.extract()),
                v.getKey().get(RowProperty.extract()),
                v.getValue())).collect(Collectors.toList());
    }

    public static Integer[][] convertIntegerHintListTo2DArray(HintList<Integer, IntVar> hints, int width, int height) {
        var result = new Integer[width][height];

        for(var h : hints) {
            var col = h.getVariable().getKey().get(ColumnProperty.extract());
            var row = h.getVariable().getKey().get(RowProperty.extract());
            var value = h.getValue();
            result[col - 1][row - 1] = value;
        }
        return result;
    }
}
