package nl.timvanoijen.logicalpuzzles.service;

import nl.timvanoijen.logicalpuzzles.core.problems.Problem;
import nl.timvanoijen.logicalpuzzles.core.problems.ProblemRepository;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingContext;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingResult;
import nl.timvanoijen.logicalpuzzles.core.solvers.SolvingStrategy;
import nl.timvanoijen.logicalpuzzles.core.variables.BoolVar;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.*;
import java.util.stream.Collectors;

public class GenerationUtils {

    public static List<BoolVar> generateBySelectAndSolve(SolvingContext solvingContext,
                                                         ProblemRepository problemRepository,
                                         SolvingStrategy solvingStrategy,
                                         List<BoolVar> freeVariables) {

        List<BoolVar> selectedVariables = new ArrayList<>();
        var problem = problemRepository.getGrandProblem();
        var rnd = solvingContext.getRandom();

        try(var executionContext = solvingContext.provideExecutionContext()) {
            while(true) { // Keep trying from the start
                selectedVariables.clear();
                int initialVersion = executionContext.newVersion();
                boolean succeeded = false;

                while (true) { // Keep adding initial values
                    SolvingResult solvingResult = solvingContext.solve(problem, solvingStrategy);
                    if (solvingResult.isSucceeded()) {
                        succeeded = true;
                        break;
                    }

                    // If failed, roll back to initial state and try again.
                    if (solvingResult.isFailed()) {
                        executionContext.rollbackToVersion(initialVersion - 1);
                        break;
                    }

                    // Select an option that is still undetermined
                    var selectableVariables = freeVariables.stream()
                            .filter(v -> v.getValue() == null)
                            .collect(Collectors.toList());

                    var selected = selectableVariables.get(rnd.nextInt(selectableVariables.size()));
                    selected.setValue(true);
                    selectedVariables.add(selected);
                }
                if (succeeded)
                    break;
            }
        }
        return selectedVariables;
    }


    /**
     * Finds a minimum subset of the provided hints that solves the
     * puzzle. The provided SolvingStrategy should be able to solve
     * the problem in case all the hints are used.
     * @param solvingContext A SolvingContext instance
     * @param problem The Problem instance to be solved. Hint variables
     *                should still be null in this problem.
     * @param solvingStrategy The SolvingStrategy to be used
     * @param hints A Map of Variable to its hint value.
     */
    public static <T, U extends Variable<T>> HintList<T,U> findMinimumSetOfHints(SolvingContext solvingContext,
                             Problem problem,
                             SolvingStrategy solvingStrategy,
                             List<? extends Hint<T,U>> hints) {

        // Verify that hint variables all still have null value.
        assert hints.stream().allMatch(v -> v.getVariable().getValue() == null);

        try (var executionContext = solvingContext.provideExecutionContext()) {
            Map<U, T> finalHints = hints.stream().collect(
                Collectors.toMap(Hint::getVariable, Hint::getValue));
            for (var hint : hints) {
                int noHintsCheckpoint = executionContext.newVersion();
                U hintVariable = hint.getVariable();

                finalHints.remove(hintVariable);
                for (var finalHint : finalHints.entrySet())
                    finalHint.getKey().setValue(finalHint.getValue());

                var result = solvingStrategy.solve(problem, solvingContext);
                assert !result.isFailed();
                if (!result.isSucceeded())
                    finalHints.put(hintVariable, hint.getValue());
                executionContext.rollbackToVersion(noHintsCheckpoint - 1);
            }

            return new HintList<>(finalHints.entrySet().stream()
                    .map(e -> new Hint<>(e.getKey(), e.getValue()))
                    .collect(Collectors.toList()));
        }
    }
}

