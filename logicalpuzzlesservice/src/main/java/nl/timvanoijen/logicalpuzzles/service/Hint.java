package nl.timvanoijen.logicalpuzzles.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

@Getter
@AllArgsConstructor
public class Hint<T, U extends Variable<T>> {
    private final U variable;
    private final T value;

    public static <T, U extends Variable<T>> Hint<T,U> fromVariable(U variable) {
        T value = variable.getValue();
        assert value != null;
        return new Hint<>(variable, value);
    }
}