package nl.timvanoijen.logicalpuzzles.service;

import nl.timvanoijen.logicalpuzzles.core.variables.Variable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class HintList<T,U extends Variable<T>> extends ArrayList<Hint<T,U>> {

    public HintList(Collection<Hint<T,U>> c) {
        super(c);
    }

    public static <T,U extends Variable<T>> HintList<T,U> fromVars(Collection<U> vars) {
        return new HintList<T,U>(vars.stream().map(v -> Hint.fromVariable(v))
                .collect(Collectors.toList()));
    }

    public List<Variable<T>> toVars() {
        return this.stream().map(Hint::getVariable)
                .collect(Collectors.toList());
    }
}
